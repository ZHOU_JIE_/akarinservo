/*
 * Copyright (c) 2015, 2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef UTILS_FILTERS_H_
#define UTILS_FILTERS_H_


typedef struct
{
    float *data;
    float *coeffs;
    int len;
} fir_filter_t;

fir_filter_t *fir_filter_init(fir_filter_t *fir, float *coeffs, int len);
float fir_update(fir_filter_t *fir, float data);

typedef struct
{
    float a0, a1, a2;
    float b1, b2;

    float x_2;
    float x_1;
    float y_2;
    float y_1;

    float cf, bw;
} iir_filter_t;

float iir_filter_update(iir_filter_t *filter, float x);

/* alpha-beta observer */
typedef struct
{
    float dt;
    float xk_1;
    float vk_1;

    float a, b;

    /* for differential algorithm */
    int64_t xm, xm1;
    float output_x_dot;
} ab_observer_t;

ab_observer_t *ab_observer_init(ab_observer_t *observer, float dt, float a, float b);

ab_observer_t *ab_observer_update(ab_observer_t *observer, float xm);

float ab_observer_differential_update(ab_observer_t *observer, int64_t xm);

typedef struct
{
	int sample_count;
	int current_sample;
	float x_sum;
	float x;
} average_sampler_t;

float average_sampler_update(average_sampler_t *sampler, float x);

average_sampler_t *average_sampler_init(average_sampler_t *sampler, int len);

iir_filter_t *iir_filter_reset(iir_filter_t *filter);

iir_filter_t *iir_filter_init(iir_filter_t *filter, float a0, float a1, float a2, float b1, float b2);

void calculate_iir_coeff(float bw, float cf,
                         float *a0, float *a1, float *a2,
                         float *b1, float *b2);

// ---------------------
typedef struct
{
    int order;
    float *z;
    float *a, *b;
} iir_filter_nth_t;

iir_filter_nth_t *iir_filter_nth_init(iir_filter_nth_t *filter, float *a, float *b, int order);

/* Form 2 IIR filter */
float iir_filter_nth_update(iir_filter_nth_t *filter, float x);
// ---------------------

typedef struct
{
    int64_t *samples, *buf_samples;
    int len;
} median_filter_t;

median_filter_t *median_filter_init(median_filter_t *filter, int len);

int64_t median_filter_update(median_filter_t *filter, int64_t sample);

#endif /* UTILS_FILTERS_H_ */
