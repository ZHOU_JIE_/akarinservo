/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <string.h>
#include "fifo.h"

#define MIN(x, y) ((x) < (y) ? (x) : (y))

int circularBufferInit(CircularBuffer *fifo, size_t size)
{
    fifo->size = size;
    fifo->in = fifo->out = 0;
    
    fifo->buf = malloc(size);
    
    if (!fifo->buf)
    {
        return -1;
    }
    
    return 0;
}

size_t circularBufferRead(CircularBuffer *fifo, uint32_t *buf, size_t len)
{
    /* FIFO is empty */
    if(circularBufferEmpty(fifo))
    {
        return 0;
    }
    
    /* read FIFO */
    
    len = MIN(len, fifo->in - fifo->out);
    size_t l = MIN(len, fifo->size - (fifo->out & (fifo->size - 1)));
    
    memcpy(buf, fifo->buf + (fifo->out & (fifo->size - 1)), l);
    memcpy(buf + l, fifo->buf, len - l);
    
    /* move read pointer */
    fifo->out += len;
    
    return len;
}

size_t circularBufferWrite(CircularBuffer *fifo, uint32_t *buf, size_t len)
{
    len = MIN(len, fifo->size - fifo->in + fifo->out);
    size_t l = MIN(len, fifo->size - (fifo->in & (fifo->size - 1)));
    
    memcpy(fifo->buf + (fifo->in & (fifo->size - 1)), buf, l);
    memcpy(fifo->buf, buf + l, len - l);
    
    /* move write pointer */
    fifo->in += len;
    
    
    return len;
}
