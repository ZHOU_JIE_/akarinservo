/*
 * Copyright (c) 2015, 2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef ALGORITHMS_H_
#define ALGORITHMS_H_

#include <inttypes.h>
#include "../kalman/kalman_filter.h"
#include "../command/string_hash_map.h"
#include "../hw/sharc_hw.h"
#include "../command/var_map.h"
#include "../utils/filters.h"

#define RAD(x) ((x) * 0.01745329251994330)
#define ABS(x) ((x) > 0 ? (x) : -(x))

/**
 *  Space Vector Pulse Width Modulation - the floating-point style
 *  This implementation is intended to be used on floating-point DSPs like
 *  SHARC DSPs. The floating-point nature maintains maximum accuracy as well as
 *  dynamic range of the algorithm itself. A large PWM period can be passed
 *  to the algorithm without any distortion being generated.
 *
 *  @param pwm_period PWM period counter
 *  @param alpha      Voltage vector alpha component
 *  @param beta       Voltage vector beta component
 *  @param Taon       Time of PWM a turns on
 *  @param Tbon       Time of PWM b turns on
 *  @param Tcon       Time of PWM c turns on
 */
void svpwm(const uint32_t pwm_period,
           const float alpha, const float beta,
           float *Taon, float *Tbon, float *Tcon);

float angle_normalize_deg(float angle);

int motor_control_read_encoder(void);

typedef struct
{
    float p, i, d;
    float p_prime, i_prime;

    float error_prev;
    float integral;
} pid_controller_t;

pid_controller_t *pid_controller_update_settings(pid_controller_t *pid, pid_settings_t *settings);

pid_settings_t *pid_controller_extract_settings(pid_controller_t *pid, pid_settings_t *settings);

pid_controller_t *pid_controller_init(pid_controller_t *pid, float p, float i, float d);

pid_controller_t *pid_controller_reset(pid_controller_t *pid);

float pi_update(pid_controller_t *pid,
		float error,
        float dt, float min, float max, float int_min, float int_max, float feed_forward);

float pid_update(pid_controller_t *pid,
        float error, float diff,
        float dt, float min, float max,
        float int_min, float int_max,
        float feed_forward);

float pi_inc_update(pid_controller_t *pid,
        float target, float measured,
        float dt, float min, float max,
        float int_min, float int_max,
        float feed_forward);

/**
 * fuzzy logic level,
 * divided into 7 levels from negative big to positive big.
 */
typedef enum
{
    NB = -3, NM = -2, NS = -1,
    ZO =  0,
    PS =  1, PM =  2, PB =  3
} fuzzy_logic_level;

typedef struct
{
	int kp_level;
	int ki_level;
	int kd_level;

	int error_level, error_change_level;

	float kp_prime, ki_prime, kd_prime;

	float p_prime_max, i_prime_max;
	float error_pb, error_change_pb;
	float kp_pb, ki_pb, kd_pb;

	float p;

} pid_fuzzy_self_tuner;

/* differentiator */
typedef struct
{
	int64_t z[5];
} differentiator_int64_t;

differentiator_int64_t *differentiator_int64_init(differentiator_int64_t *d);
float differential_int64(differentiator_int64_t *diff, int64_t x);

typedef struct
{
	float z[5];
} differentiator_float_t;

differentiator_float_t *differentiator_float_init(differentiator_float_t *d);
float differential_float(differentiator_float_t *diff, float x);

/**
 *  find the fuzzy logic level from a given fuzzy logic rule using given
 *  error and change of error
 *
 *  @param e                error
 *  @param ec               change of error
 *  @param fuzzy_logic_rule fuzzy logic rule table
 *
 *  @return fuzzy logic level
 */
int fuzzy_logic_find(int e, int ec, int *fuzzy_logic_rule);

/**
 *  calculate the membership of the quantity x from the given boundary
 *
 *  @param nb negative big boundary
 *  @param pb positive big boundary
 *  @param x  quantity x
 *
 *  @return fuzzy logic membership
 */
int fuzzy_logic_membership(float nb, float pb, float x);

/**
 *  defuzzify a given fuzzy level into quantity
 *
 *  @param nb    negative big
 *  @param pb    positive big
 *  @param level fuzzy logic level
 *
 *  @return defuzzified quantity
 */
float fuzzy_logic_defuzzify(float nb, float pb, int level);

void print_variable_record(float *buffer, int len);

static inline float var_range_clamp(float x, float min, float max)
{
    if(x > max) return max;
    if(x < min) return min;

    return x;
}

uint32_t
crc32(uint32_t crc, const void *buf, int size);

#endif /* ALGORITHMS_H_ */
