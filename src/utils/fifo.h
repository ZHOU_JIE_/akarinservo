/*
 * Copyright (c) 2015, 2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef fifo_h
#define fifo_h

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

/*
 * CircularBuffer(FIFO) - a circular buffer implementation
 *
 * This implementation allows blocking and non-blocking read.
 */
typedef struct CircularBuffer
{
    size_t size;
    size_t in, out;
    
    uint32_t *buf;
    
} CircularBuffer;

/**
 *  Test if a circular buffer is empty
 *
 *  @param fifo Pointer to the circular buffer
 *
 *  @return true if is empty, false otherwise
 */
static inline bool circularBufferEmpty(CircularBuffer *fifo)
{
    return fifo->in == fifo->out;
}

/**
 *  Test if a circular buffer is full
 *
 *  @param fifo Pointer to the circular buffer
 *
 *  @return true if is full, false otherwise
 */
static inline bool circularBufferFull(CircularBuffer *fifo)
{
    return (fifo->in - fifo->out) == fifo->size;
}

/**
 *  Reset a circular buffer
 *
 *  @param fifo Pointer to the circular buffer
 */
static inline void circularBufferReset(CircularBuffer *fifo)
{
    fifo->in = fifo->out = 0;
}

int circularBufferInit(CircularBuffer *fifo, size_t size);

size_t circularBufferRead(CircularBuffer *fifo, uint32_t *buf, size_t len);

size_t circularBufferWrite(CircularBuffer *fifo, uint32_t *buf, size_t len);

#endif /* fifo_h */
