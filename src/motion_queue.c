/*
 * Copyright (c) 2015, 2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "motion_queue.h"

void motion_queue_init(motion_queue_t *Q,int maxsize)
{
    Q->front = 0;
    Q->rear = 0;
    Q->count = 0;
    Q->maxsize = maxsize;
}

bool motion_queue_full(motion_queue_t *Q)
{
    if(Q->front == (Q->rear + 1) % Q->maxsize)
    {
    	return true;
    }
    else
    {
        return false;
    }
}

bool motion_queue_empty(motion_queue_t *Q)
{
    if(Q->front == Q->rear)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool motion_enqueue(motion_queue_t *Q, motion_t val)
{
    if(motion_queue_full(Q))
    {
        return false;
    }
    else
    {
        Q->p[Q->rear] = val;
        Q->rear = (Q->rear + 1) % Q->maxsize;
        Q->count++;

        return true;
    }
}

bool motion_dequeue(motion_queue_t *Q, motion_t *val)
{
    if(motion_queue_empty(Q))
    {
        return false;
    }
    else
    {
        *val = Q->p[Q->front];
        Q->front=(Q->front + 1) % Q->maxsize;
        Q->count--;

        return true;
    }
}

motion_t motion_make(int id, int64_t target, float target_acc, float target_vel)
{
    motion_t motion = {
        .id = id,
        .target = target,
        .target_acc = target_acc,
        .target_vel = target_vel
    };
    
    return motion;
}
