/*
 * Copyright (c) 2015, 2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "hw/sharc_hw.h"
#include "motor_control.h"
#include "command/command.h"
#include <inttypes.h>
#include <stdint.h>

#include "kalman/kalman_filter.h"
#include "hw/encoder.h"

modbus_cmd_handler_func *modbus_handlers[MODBUS_HANDLERS_MAX];

uint32_t modbus_crc16(uint8_t *buf, int len)
{
	uint32_t crc = 0xFFFF;

	for (int pos = 0; pos < len; pos++)
	{
		crc ^= (uint8_t)buf[pos];

		for (int i = 8; i != 0; i--)
		{
			if ((crc & 0x0001) != 0)
			{
				crc >>= 1;
				crc ^= 0xa001;
			}
			else
			{
				crc >>= 1;
			}
		}
	}

	return crc & 0xFFFF;
}

float modbus_ieee754_parse(uint8_t *buf)
{
	uint32_t word = 0;

	for(int j = 0; j < 4; j++)
	{
		word <<= 8;
	    word |= buf[j];
	}

	float val = *(float *)(&word);
	return val;
}

float modbus_int32_t_parse(uint8_t *buf)
{
	uint32_t word = 0;

	for(int j = 0; j < 4; j++)
	{
		word <<= 8;
	    word |= buf[j];
	}

	return word;
}

int modbus_handler_cmd_go(motor_control_session_t *session, uint8_t *data)
{
	/* motion go command */
	float target, acc, vel;
	modbus_extract_args((uint8_t *)data, "fff", &target, &acc, &vel);

	motion_cmd_go(session, target, acc, vel);

	return 0;
}

int modbus_handler_cmd_trigger(motor_control_session_t *session, uint8_t *data)
{
	motion_cmd_trigger(session);

	return 0;
}

uint8_t *modbus_extract_args(uint8_t *data, char *arg_fmt, ...)
{
	uint8_t *curr_ptr = data + 2;

	va_list ap;
    va_start(ap, arg_fmt);

     while (*arg_fmt)
     {
         switch(*arg_fmt)
         {
             case 'd':
            	 *(int *)va_arg(ap, int *) = modbus_int32_t_parse(curr_ptr);
            	 curr_ptr += 4;
            	 break;

             case 'f':
            	 *(float *)va_arg(ap, float *) = modbus_ieee754_parse(curr_ptr);
            	 curr_ptr += 4;
            	 break;
         }
         arg_fmt++;
     }
     va_end(ap);


	return data;
}

void modbus_handlers_init(void)
{
	/* clear handlers */
	for(int i = 0; i < MODBUS_HANDLERS_MAX; i++)
	{
		modbus_handlers[i] = NULL;
	}

	/* register handlers */
	modbus_handler_register(MODBUS_FUNC_MOTION_GO, modbus_handler_cmd_go);
	modbus_handler_register(MODBUS_FUNC_MOTION_TRIGGER, modbus_handler_cmd_trigger);
}

void modbus_handler_register(uint8_t func_code, modbus_cmd_handler_func handler)
{
	modbus_handlers[func_code] = handler;
}

void modbus_handle(char *data, int len)
{
	/* we allow maximum 128 data length.
	 * modbus requires at least 4 bytes of data. */
	if(len > 128 || len < 4)
	{
		return;
	}

	/* | slave addr | func code |     data     |    CRC    |
	 * |   1 byte   |   1 byte  | 0 - 255 byte | low, high |
	 * */

	int addr 	  = data[0];
	int func_code = data[1];
	int crc16     = (data[len - 1] & 0xff) << 8 | (data[len - 2] & 0xff);

	/* return if address mismatches */
	if(addr != mc_session.controller_configs.local_address)
	{
		return;
	}

	/* calculate CRC16 */
	int crc16_calc = modbus_crc16((uint8_t *)data, len - 2);

	/* return if checksum mismatch */
	if(crc16 != crc16_calc)
	{
		return;
	}

	/* call the actual function if it is registered */
	if(modbus_handlers[func_code] != NULL)
	{
		int ret = modbus_handlers[func_code](&mc_session, (uint8_t *)data);

		if(ret == 0)
		{
			// TODO: reply OK
		}
	}
}
