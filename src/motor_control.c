/*
 * Copyright (c) 2015, 2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "hw/sharc_hw.h"
#include "motor_control.h"
#include <inttypes.h>

#include "kalman/kalman_filter.h"
#include "command/command.h"

int echo_enable = 0;
int debug_enable = 0;

StringHashMap *command_list;
StringHashMap *variable_map;

motor_control_session_t mc_session;
scope_session_t 		scope_session;
motion_queue_t 			motion_queue;

float home_vel_max = 5.0f;
float home_vel = 0.0f;
float home_acc = 0.001f;
float home_decel = 0.01f;
int64_t home_click = 0;
int home_found = 0;

void motor_control_modulate(float vd, float vq, float sint, float cost)
{
    /* Inverse Park Transformation */
    const float beta  = vd * sint + vq * cost;
    const float alpha = vd * cost - vq * sint;

    /* run SVPWM algorithm */
    float aon, bon, con;
    svpwm(mc_pwm_period, alpha, beta, &aon, &bon, &con);

    /* update PWM comparator, we use integer thresholds */
    update_pwm_comp((int)aon, (int)bon, (int)con);
}

static int fuzzy_logic_rule_kp[] = {
    PB, PB, PM, PM, PS, ZO, ZO,
    PB, PB, PM, PS, PS, ZO, NS,
    PM, PM, PM, PS, ZO, NS, NS,
    PM, PM, PS, ZO, NS, NM, NM,
    PS, PS, ZO, NS, NS, NM, NM,
    PS, ZO, NS, NM, NM, NB, NB,
    ZO, ZO, NM, NM, NM, NB, NB
};

static int fuzzy_logic_rule_ki[] = {
    NB, NB, NM, NM, NS, ZO, ZO,
    NB, NB, NM, NS, NS, ZO, ZO,
    NB, NM, NS, NS, ZO, PS, PS,
    NM, NM, NS, ZO, PS, PM, PM,
    NM, NS, ZO, PS, PS, PM, PB,
    ZO, ZO, PS, PS, PM, PB, PB,
    ZO, ZO, PS, PM, PM, PB, PB
};

static int fuzzy_logic_rule_kd[] = {
    PS, NS, NB, NB, NB, NM, PS,
    PS, NS, NM, NM, NM, NS, ZO,
    ZO, NS, NM, NM, NS, NS, ZO,
    ZO, NS, NS, NS, ZO, NS, ZO,
    ZO, ZO, ZO, ZO, PS, ZO, ZO,
    PB, PS, PS, PS, PS, PS, PB,
    PB, PM, PM, PM, NS, PS, PB
};

static inline
void motor_control_self_tuning(
        pid_fuzzy_self_tuner *tuner,
        pid_controller_t *pid,
        float error, float derror
) {

	return;

    /* now calculate fuzzy logic level of error and change of error
     * using the membership function */
    tuner->error_level =
            fuzzy_logic_membership(0, tuner->error_pb, fabs(error));
    tuner->error_change_level =
            fuzzy_logic_membership(-tuner->error_change_pb, tuner->error_change_pb, derror);

    /* find the fuzzy rule using the calculated error level and error change level */
    tuner->kp_level = fuzzy_logic_find(tuner->error_level, tuner->error_change_level, fuzzy_logic_rule_kp);
    tuner->ki_level = fuzzy_logic_find(tuner->error_level, tuner->error_change_level, fuzzy_logic_rule_ki);
    tuner->kd_level = fuzzy_logic_find(tuner->error_level, tuner->error_change_level, fuzzy_logic_rule_kd);

    /* defuzzify output logic, we get pk_prime, which is the correction term */
    tuner->kp_prime = fuzzy_logic_defuzzify(-tuner->kp_pb, tuner->kp_pb, tuner->kp_level);
    tuner->ki_prime = fuzzy_logic_defuzzify(-tuner->ki_pb, tuner->ki_pb, tuner->ki_level);
    tuner->kd_prime = fuzzy_logic_defuzzify(-tuner->kd_pb, tuner->kd_pb, tuner->kd_level);

    pid->p_prime -= tuner->kp_prime;
    pid->i_prime += tuner->ki_prime;

    if(pid->p_prime > tuner->p_prime_max) pid->p_prime = tuner->p_prime_max;
    if(pid->p_prime < -tuner->p_prime_max) pid->p_prime = -tuner->p_prime_max;

    if(pid->i_prime > tuner->i_prime_max) pid->i_prime = tuner->i_prime_max;
    if(pid->i_prime < -tuner->i_prime_max) pid->i_prime = -tuner->i_prime_max;

    //pid->p_prime = iir_filter_update(&session.tuner_filter, pid->p_prime);

    tuner->p = pid->p + pid->p_prime;
}

int motion_type = 0;
int motion_pause_length = 0;
int motion_pause_count = 0;
int motion_trigger = 0;

void variable_scope_tick(motor_control_session_t *session, scope_session_t *scope)
{
	scope->count_div++;

    if(scope->count_div == scope->var_scope_div)
    {
    	scope->count_div = 0;

        if(scope->var_scope_sample_len < scope->var_scope_trigger_count && !scope->var_scope_triggered)
        {
            int index = scope->var_scope_sample_len++;

            scope->var1_scope_buffer[index] = extract_whatever(scope->var_scope_var1);

            if(scope->var_scope_var2 != NULL)
            {
            	scope->var2_scope_buffer[index] = extract_whatever(scope->var_scope_var2);
            }

            if(scope->var_scope_sample_len == scope->var_scope_trigger_count - 1)
            {
            	scope->var_scope_triggered = 1;
            }
        }
    }
}


void motion_control_execute(motor_control_session_t *session)
{
    if(session->state == CONTROL_POSITION &&
       !motion_queue_empty(&motion_queue) &&
       motion_trigger > 0)
    {
        /* dequeue a motion from the queue and execute it */
        motion_t motion;
        motion_dequeue(&motion_queue, &motion);

        /* all motion sequence extracted, reset the trigger */
        if(motion_queue_empty(&motion_queue))
        {
            motion_trigger = 0;
        }

        motion_type = motion.motion_type;

        /* setup motion controller */
        motion_prepare(&session->motion,
        		angular_pos_to_float(
        		angular_pos_plus(session->rot_pos_meas, session->x_offset, PERIOD_REVOLUTION),
				PERIOD_REVOLUTION),

        		//angular_pos_to_float(session->rot_pos_meas, 16384.0f) + session->x_offset,
                motion.target, motion.target_acc, motion.target_vel);

        motion_pause_length = motion.duration;

        /* now off we go */
        session->state = CONTROL_MOTION;
    }

    if(session->state == CONTROL_MOTION)
    {
        switch(motion_type)
        {
            case MOTION_GO:
            {
                /* run motion controller */
                if(motion_control_tick(&session->motion))
                {
                    /* set the rotor position to the position demanded by
                     * the motion controller */
                	angular_pos_t output = angular_pos_from_float(session->motion.pos_output, PERIOD_REVOLUTION);
                	session->rot_pos_set = angular_pos_difference(output, session->x_offset, PERIOD_REVOLUTION);

                    LED_BLUE_LOW();
                }
                else
                {
                    /* motion control ends, go back to CONTROL_RUN mode */
                    session->state = CONTROL_POSITION;

                    LED_BLUE_HIGH();

                    /* set the rotor position to target position */
                	angular_pos_t output = angular_pos_from_float(session->motion.target_pos, PERIOD_REVOLUTION);
                	session->rot_pos_set = angular_pos_difference(output, session->x_offset, PERIOD_REVOLUTION);
                }
            }
            break;

            case MOTION_PAUSE:
            {
                motion_pause_count++;

                if(motion_pause_count == motion_pause_length)
                {
                    motion_pause_count = 0;
                    /* motion control ends, go back to CONTROL_RUN mode */
                    session->state = CONTROL_POSITION;

                    LED_BLUE_HIGH();
                }
            }
            break;

            case MOTION_ZERO:
            {
            	execute_command(&mc_session, command_list, "zero");

            	session->state = CONTROL_POSITION;
            	break;
            }

            case MOTION_HOME:
            {
            	if(mc_session.dio_home_switch == 0)
            	{
            		home_vel += home_acc;
            		if(home_vel >= home_vel_max)
            		{
            			home_vel = home_vel_max;
            		}

            		session->rot_pos_set = angular_pos_plus_pos(session->rot_pos_set, home_vel, PERIOD_REVOLUTION);
            	}
            	else
            	{
             		if(home_found == 0)
                	{
                		home_click = angular_pos_to_float(session->rot_pos_set, PERIOD_REVOLUTION);
               			home_found = 1;
               		}

            		home_vel -= home_decel;
            		session->rot_pos_set = angular_pos_plus_pos(session->rot_pos_set, home_vel, PERIOD_REVOLUTION);
            		if(home_vel <= 0.0f)
            		{
            			session->x_offset = angular_pos_from_float(-home_click, PERIOD_REVOLUTION);

            			session->state = CONTROL_POSITION;
            		}
            	}

            }
            break;

            default:
            break;
        }


    }
}

float tq_ff_coeff = 0.0f, tq_ff_phase = 0.0f;

void motor_control_read_sensors(motor_control_session_t *session)
{
    /* activate ADC conversion */
    ADC_CONVST_LOW();

    /* read encoder angle once the ADC conversion starts */
    int encoder_mechanical_angle = motor_control_read_encoder();
    ADC_CONVST_HIGH();

    adc_read_data(&session->hall_a_raw, &session->hall_b_raw);

    session->hall_a_raw = median_filter_update(&session->current_filter_a, session->hall_a_raw);
    session->hall_b_raw = median_filter_update(&session->current_filter_b, session->hall_b_raw);

    /* current sensor calibration constants */
    session->hall_a_raw += session->controller_configs.hall_a_bias;
    session->hall_b_raw += session->controller_configs.hall_b_bias;

    /* current sensor B is installed in the opposite direction */
    session->hall_b_raw = -session->hall_b_raw;

    /* convert readings to Ampere */
    session->current_a = session->hall_a_raw * session->controller_configs.hall_a_coeff;
    session->current_b = session->hall_b_raw * session->controller_configs.hall_b_coeff;

//#define __SIMULATE_ENCODER_MISSCODE

#ifdef __SIMULATE_ENCODER_MISSCODE
    if(rand() % 100 == 0)
    {
    	encoder_mechanical_angle = 16384;
    }
#endif

    session->encoder_raw = encoder_mechanical_angle;

    /* here we calculate the angular 'distance' instead of rotor angle.
     * we first take the differential of the rotor angle and perform a
     * periodic transform, then calculate its integral again. */
    int encoder_delta = (encoder_mechanical_angle - session->mechanical_angle_prev);// * ENCODER_RATIO;

    /* periodic transform */
    if(encoder_delta >  POS_HALF_REVOLUTION) encoder_delta -= POS_PER_REVOLUTION;
    if(encoder_delta < -POS_HALF_REVOLUTION) encoder_delta += POS_PER_REVOLUTION;

    /* add measured delta to encoder count */
    session->encoder_count += encoder_delta;

    // TODO: need a better way to do this
    /* calculate rotor accumulated position */
    /*const angular_pos_t rot_pos_meas_new =
    		angular_pos_plus_pos(session->rot_pos_meas, encoder_delta, PERIOD_REVOLUTION);
     */
    const angular_pos_t rot_pos_meas_new = angular_pos_from_float(session->encoder_count, PERIOD_REVOLUTION);

    /* encoder misscode detection:
     * considered misscode if the difference between previous sampled encoder position
     * and current encoder position is greater than ENCODER_MISSCODE_CHECK_THRES.
     * ignore current sample if encoder misscode is encountered. */
	if(session->encoder_check_enabled &&
	   (ABS(angular_pos_difference_float(rot_pos_meas_new, session->rot_pos_meas_prev, PERIOD_REVOLUTION)) >
	    ENCODER_MISSCODE_CHECK_THRES))
	{
		/* misscode */
		session->encoder_misscode_count++;
	}
	else
	{
		session->rot_pos_meas = rot_pos_meas_new;
		session->rot_pos_meas_prev = session->rot_pos_meas;
		session->mechanical_angle_prev = encoder_mechanical_angle;
	}

    /* calculate angular velocity, angular acceleration and
     * angular jerk using 3 differentiators */
    float ang_vel_raw = differential_angular_pos(&session->pos_diff, session->rot_pos_meas) * CONTROL_FREQUENCY;
    float ang_acc_raw = differential_float(&session->acc_diff, ang_vel_raw);
    float ang_jerk_raw = differential_float(&session->jerk_diff, ang_acc_raw);

    /* estimate the angular velocity using a Kalman filter.
     * unit is revolution per second */
    session->angular_state = insKalmanFilterUpdate(
        &session->angular_vel_kalman_filter,
        insVector3fMake(ang_vel_raw, ang_acc_raw, ang_jerk_raw),
        VECTOR3F_ZERO
    );

    /* calculate angular velocity in degrees per second
     * x is angular velocity,
     * y is angular acceleration,
     * z is angular jerk. */
    session->rot_vel_meas = session->angular_state.x * DEG_PER_POS;
    session->rot_vel_meas = iir_filter_update(&session->vel_filter, session->rot_vel_meas);

    /* calculate mechanical angle range [0.0, 360.0) */
    const float rotor_mechanical_angle = encoder_mechanical_angle * DEG_PER_POS;
    session->electric_angle = (rotor_mechanical_angle * session->encoder_configs.poles) + session->encoder_configs.encoder_offset;

    /* normalize electric angle to [0.0, 360.0) */
    session->electric_angle = angle_normalize_deg(session->electric_angle);
}

typedef struct
{
	float *map;
	int len;
} cogging_torque_map_t;



float __tm[] = {
#include "c:\\torque.csv"
};

cogging_torque_map_t cogging_torque_map = {
		.map = __tm,
		.len = 328
};

float calculate_cogging_torque(float pos, float *cog_torque, int len)
{
    float index_frac = pos / PERIOD_REVOLUTION * len;

    int index = index_frac;
    int index_prev = index - 1;

    float t = index_frac - index;

    /* wrap around */
    if(index_prev < 0)
    {
        index_prev = len - 1;
    }

    if(index > len - 1)
    {
        index = len - 1;
    }

    return cog_torque[index] * t + (1.0 - t) * cog_torque[index_prev];
}

void motor_control_servo_loop(motor_control_session_t *session, float sint, float cost, float angle_rad)
{

    session->pos_set_d = 0.0f;

    if(session->state == CONTROL_POSITION  ||
       session->state == CONTROL_VELOCITY  ||
       session->state == CONTROL_MOTION)
    {
        session->tick_count++;

        if(session->state != CONTROL_VELOCITY)
        {
            motion_control_execute(session);

            /* position PID controller self tuning using fuzzy logic */
            /* calculate error */
            session->ang_error =
            		angular_pos_difference_float(
            				session->rot_pos_set, session->rot_pos_meas,
							PERIOD_REVOLUTION
			);

        	session->ang_error = iir_filter_nth_update(&session->vel_command_filter, session->ang_error) * 0.06745527388;//* 0.005542717210;

            /* calculate change of error */
            session->ang_derror = differential_float(
                    &session->ang_error_diff,
					session->ang_error
            );

            /* tune the controller based on error and change of error
             * using fuzzy logic rules */
            motor_control_self_tuning(
                    &session->pos_pid_tuner,
                    &session->position_controller,
                    session->ang_error, session->ang_derror
            );

            // TODO: use diff ab-observer
            /* observe the velocity of the position command */
            ab_observer_update(&session->vel_ff_observer, angular_pos_to_float(session->rot_pos_set, PERIOD_REVOLUTION));
            session->pos_set_d = session->vel_ff_observer.vk_1;

            /* calculate velocity feed-forward term and clamp it within a range */
            const float vel_feedforward_term = var_range_clamp(
                     session->encoder_configs.vel_feedforward * session->pos_set_d,
                    -session->encoder_configs.rotor_vel_limit, session->encoder_configs.rotor_vel_limit
            );

            /* -------------------------
             * PID control loop
             * POSITION LOOP
             * -------------------------
             *  */
            session->rot_vel_set =

            pid_update(
                    &session->position_controller,
					session->ang_error, /* reference and measured term */
                    session->ang_derror * DEG_PER_POS,       /* change of error */
                    CONTROL_LOOP_TICK_TIME,                  /* loop period */
                   -session->encoder_configs.rotor_vel_limit,        /* output saturation range */
                    session->encoder_configs.rotor_vel_limit,

                    /* integral saturation range */
                   -session->rot_ang_vel_integral_max, session->rot_ang_vel_integral_max,
                    vel_feedforward_term                     /* feed forward term */
            );

            //session->rot_vel_set = iir_filter_nth_update(&session->vel_command_filter, session->rot_vel_set)* 0.005542717210;

            // XXX: filter test
            for(uint32_t i = 0; i < SESSION_IIR_FILTER_COUNT; i++)
            {
            	/* if filter enabled */
            	if(session->enabled_iir_notch_filters & (1 << i))
            	{
            		/* apply the filter */
            		session->rot_vel_set = iir_filter_update(&session->notch_filters[i], session->rot_vel_set);
            	}
            }
        }
        else
        {
            session->rot_vel_set = session->rotor_angular_vel_control;
        }

        /* velocity PID controller self tuning */
        /* calculate velocity error */
        session->vel_error = session->rot_vel_set - session->rot_vel_meas;

        /* calculate change of velocity error */
        session->vel_derror = differential_float(&session->vel_error_diff, session->vel_error);

        /* self tuning */
        motor_control_self_tuning(
                &session->vel_pid_tuner,
                &session->velocity_controller,
                session->vel_error, session->vel_derror
        );

        // TODO: use diff ab-observer
        /* observe the velocity of the position command */
        ab_observer_update(&session->torque_ff_observer, session->pos_set_d);
        session->pos_set_dd = session->torque_ff_observer.vk_1;

        /* cogging torque compensation using static torque-position map */
        /* Cogging Torque Ripple Minimization via Position-Based Characterization
         * Matthew Piccoli and  Mark Yim */
        float torque_cogging_compensation_term =
        		tq_ff_coeff * calculate_cogging_torque
    			(
    				/* convert encoder reading to [0, 16384) */
    			    (float)session->encoder_raw * ENCODER_RATIO,
    				cogging_torque_map.map, cogging_torque_map.len
    			);

        /* calculate torque feed-forward term and clamp it within a range */
        float torque_feedforward_term = var_range_clamp
        (
        	/* torque feed-forward from d(vel)/dt */
            session->encoder_configs.torque_feedforward * session->pos_set_dd -
			torque_cogging_compensation_term,
            -session->encoder_configs.torque_limit, session->encoder_configs.torque_limit
        );

        session->torque_meas_anticogged = session->torque_meas - torque_cogging_compensation_term;

        /* -------------------------
         * PID control loop
         * VELOCITY LOOP
         * -------------------------
         * the target rotor angular velocity is used as the feed forward term */
        session->torque_set = -pi_update(
                &session->velocity_controller,
                session->rot_vel_set -            /* reference term */
                session->rot_vel_meas,            /* measured term */
                CONTROL_LOOP_TICK_TIME,           /* loop period */
               -session->encoder_configs.torque_limit,      /* output saturation range */
                session->encoder_configs.torque_limit,
			   -50.0,
			    50.0,/* integral saturation range */
                torque_feedforward_term           /* feed forward term */
        );

        /* -------------------------
         * PID control loop
         * TORQUE LOOP
         * -------------------------
         *  */
        session->current_cmd = pi_update(
                        &session->torque_controller,
                        session->torque_set -         /* reference term */
                        session->torque_meas,         /* measured term */
                        CONTROL_LOOP_TICK_TIME,       /* loop period */
                       -0.99,                         /* output saturation range */
                        0.99,                         /* integral saturation range */
                       -0.95, 0.95,
					   0.0f                           /* feed forward term */
        );

        //session->current_cmd = iir_notch_filter_update(&session->filter, session->current_cmd);

        motor_control_modulate(session->flux_cmd, session->current_cmd, sint, cost);
    }
    else if(session->state == CONTROL_TORQUE)
    {
        session->current_cmd = pi_update(
                        &session->torque_controller,
                        session->free_run_torque -     /* reference term */
                        session->torque_meas,         /* measured term */
                        CONTROL_LOOP_TICK_TIME,       /* loop period */
                       -0.99,               /* output saturation range */
                        0.99,               /* integral saturation range */
                       -0.6, 0.6,

                        /* feed forward term */
                        0);


        motor_control_modulate(session->flux_cmd, session->current_cmd, sint, cost);
    }
    else if(session->state == CONTROL_SET_ANGLE)
    {
    	float ang = RAD(session->set_angle);

        motor_control_modulate(0, session->free_run_torque, cosf(ang), sinf(ang));
    }
    else if(session->state == CONTROL_FB)
    {
        motor_control_modulate(session->flux_cmd, session->free_run_torque, sint, cost);
    }
    else
    {
        /* CONTROL_PAUSED mode, disable all PWM outputs */
        update_pwm_comp(0, 0, 0);
    }

    /* calculate displayed angular velocity and torque using average filters */
    session->ang_vel_display = average_sampler_update(&session->ang_vel_sampler, session->rot_vel_meas);
    session->torque_display = average_sampler_update(&session->torque_sampler, session->torque_meas);

    /* out-of-control detection */
    if(abs(session->ang_error) > session->controller_configs.out_of_control_thres)
    {
    	CONTROL_STOP(session);
    	SET_ERROR(ERROR_OUT_OF_CONTROL);
    }
    else
    {
    	CLR_ERROR(ERROR_OUT_OF_CONTROL);
    }

}

void motor_control_execute(motor_control_session_t *session)
{
    if(session->state == CONTROL_VFD)
    {
	    /* skip the control loop if session is stopped */
	    if(session->stop)
	    {
	        update_pwm_comp(0, 0, 0);
	        goto control_loop_break;
	    }

	    session->tick_count++;

	    session->vfd.electric_angle_inc = session->vfd.frequency * FREQ_INC_RATIO;
    	session->vfd.electric_angle += session->vfd.electric_angle_inc;

    	if(session->vfd.electric_angle >= M_2PI) session->vfd.electric_angle -= M_2PI;
    	if(session->vfd.electric_angle <  0.0f)	 session->vfd.electric_angle += M_2PI;

    	motor_control_modulate(0.0f,
    			session->vfd.torque_ratio,
    			sinf(session->vfd.electric_angle),
				cosf(session->vfd.electric_angle)
		);
    }
    else
    {
    	motor_control_read_sensors(session);

	    /* electric angle in rad, this is used in the following calculations */
	    const float angle_rad = RAD(session->electric_angle);
	    const float cost = cosf(angle_rad);
	    const float sint = sinf(angle_rad);

	    /* Clarke transformation
	     * current 3->2 phase transform */
	    const float i_alpha = session->current_a;
	    const float i_beta = (session->current_a + 2.0 * session->current_b) / 1.7320508075688772;

	    /* Park transformation
	     * The two-axis orthogonal stationary reference frame quantities are
	     * transformed into rotating reference frame quantities */
	    /* Torque is in Newton-Meter */
	    session->motor_current_meas = -i_alpha * sint + i_beta * cost;

	    /* calculate magnetic flux and torque */
	    session->flux_meas   = i_alpha * cost + i_beta * sint;
	    session->torque_meas = session->motor_current_meas * session->encoder_configs.torque_coeff;

	    session->motor_current_meas_filtered =
	    		average_sampler_update(&session->motor_current_sampler, session->motor_current_meas);

	    /* skip the control loop if session is stopped */
	    if(session->stop)
	    {
	        update_pwm_comp(0, 0, 0);
	        goto control_loop_break;
	    }


        /* since we are using synchronous motor here, the magnetic flux
         * of the rotor should always be zero to achieve maximum efficiency.
         * before we start calculating any other control loops, the magnetic
         * flux control loop is calculated. */

        /* -------------------------
         * PID control loop
         * MAGNETIC FLUX LOOP
         * -------------------------
         * this keeps the magnetic flux id to zero */
        session->flux_cmd = pi_update(
                &session->mag_flux_controller,
                -session->flux_meas,            /* measured term */
                CONTROL_LOOP_TICK_TIME,         /* loop period */

                /* output saturation range */
               -session->flux_cmd_max, session->flux_cmd_max,

                /* integral saturation range */
               -session->flux_cmd_integral_max, session->flux_cmd_integral_max,
                0.0f                            /* feed forward term */
        );

    	motor_control_servo_loop(session, sint, cost, angle_rad);
    }


    /* torque protect detection */
    /*if(abs(session->torque_meas) > 1.0f)
    {
    	session->stop = 1;
    	SET_ERROR(ERROR_TORQUE_LIM_PROCTECT);
    }
    else
    {
    	CLR_ERROR(ERROR_TORQUE_LIM_PROCTECT);
    }*/

    if(abs(session->motor_current_meas_filtered) > session->encoder_configs.motor_current_protect_limit)
    {
    	CONTROL_STOP(session);
    	SET_ERROR(ERROR_OVER_CURRENT);
    }
    else
    {
    	CLR_ERROR(ERROR_OVER_CURRENT);
    }

control_loop_break:

	variable_scope_tick(session, &scope_session);

    return;
}
