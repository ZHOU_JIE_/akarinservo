/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

/**
 *  The is a key-value pair hash map implementation.
 *  Key is of type char *, and value is of type void *.
 */

#ifndef __STRING_HASH_MAP_H__
#define __STRING_HASH_MAP_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>
#include <stddef.h>

typedef struct StringHashMapEntry
{
    struct StringHashMapEntry *next;
    
    // TODO: change to string object?
    char key[32];
    void *value;
    
} StringHashMapNode;

typedef struct
{
    StringHashMapNode *root;
    int hashArraySize;
} StringHashMap;

/**
 *  Create a new string hash map
 *
 *  @param hashArraySize The hash arrary size
 *
 *  @return The pointer to the new string hash map
 */
StringHashMap *stringHashMapCreate(int hashArraySize);

/**
 *  Find the value stored in the map by a given key
 *
 *  @param map The pointer to the map
 *  @param key The key
 *
 *  @return The hash map node containing the key and value
 */
StringHashMapNode *stringHashMapFind(StringHashMap *map, char *key);

/**
 *  Insert a new key-value pair to the map
 *
 *  @param map   The pointer to the map
 *  @param key   The key
 *  @param value The value
 *
 *  @return The pointer to the input map
 */
StringHashMap *stringHashMapInsert(StringHashMap *map, char *key, void *value);

/**
 *  Remove a key-value pair from the map. Note: The key-value pair's memory
 *  is not released, use free() after that
 *
 *  @param map The pointer to the map
 *  @param key The key
 *
 *  @return The pointer to the key-value pair
 */
StringHashMapNode *stringHashMapRemove(StringHashMap *map, char *key);

/**
 *  Release the memory allocated to the map
 *
 *  @param map The pointer to the map
 */
void stringHashMapDestroy(StringHashMap *map);

/**
 *  Print the map structure
 *
 *  @param map The pointer to the map
 */
void stringHashMapShowMap(StringHashMap *map);

/* iterator */

typedef struct
{
    size_t maxIndex, index;
    StringHashMapNode *root, *current;
    
} StringHashMapIterator;

/**
 *  Get the next element in the map
 *
 *  @param it The pointer to the iterator
 *
 *  @return The node
 */
StringHashMapNode *stringHashMapNext(StringHashMapIterator *it);

/**
 *  Get the first item in the map
 *
 *  @param map The pointer to the map
 *  @param it  The pointer to the iterator
 *
 *  @return The pointer to the input iterator
 */
StringHashMapIterator *stringHashMapBegin(StringHashMap *map, StringHashMapIterator *it);

/**
 *  Test if the iterator reached the end of the map
 *
 *  @param it The pointer to the iterator
 *
 *  @return True if there's no next item, false otherwise
 */
static inline
bool stringHashMapEnd(StringHashMapIterator *it)
{
    return it->current->next ? false : true;
}

#endif /* defined(__STRING_HASH_MAP_H__) */
