/*
 * Copyright (c) 2015, 2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __VAR_MAP_H
#define __VAR_MAP_H

#include "../angular_pos.h"

typedef enum
{
    VAR_INT32,
    VAR_FLOAT32,
    VAR_INT64,
	VAR_ANGULAR_POS,
} variable_type;

typedef struct
{
    void *ptr;
    variable_type type;
    int read_only;
} aliased_variable_t;

static inline float extract_whatever(aliased_variable_t *var)
{
    if(var->type == VAR_INT32)
    {
        return (float)(*(int *)var->ptr);
    }
    if(var->type == VAR_INT64)
    {
        return (float)(*(int64_t *)var->ptr);
    }
    if(var->type == VAR_FLOAT32)
    {
        return *(float *)var->ptr;
    }
    if(var->type == VAR_ANGULAR_POS)
    {
    	return angular_pos_to_float(*(angular_pos_t *)var->ptr, PERIOD_REVOLUTION);
    }

    return 0.0f;
}

#endif /* __VAR_MAP_H */
