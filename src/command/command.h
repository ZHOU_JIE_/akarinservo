/*
 * Copyright (c) 2015, 2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __COMMAND_H
#define __COMMAND_H

#include "string_hash_map.h"
#include "../motor_control.h"
#include "var_map.h"

#define COMMAND(_name) __cmd_##_name
#define COMMAND_HANDLER(_name) int __cmd_##_name(motor_control_session_t *session, int argc, char *argv[])

typedef int command_handler_func(motor_control_session_t *, int, char *[]) ;

command_handler_func *find_command(StringHashMap *command_list, char *cmd);

int execute_command(motor_control_session_t *session, StringHashMap *command_list, char *cmd_in);

extern StringHashMap *variable_map;

#define MAX_VARIABLE_COUNT 128
static int variable_count = 0;
static aliased_variable_t variable_pool[MAX_VARIABLE_COUNT];

/**
 *  register a variable to the map
 *
 *  @param var  pointer to the variable
 *  @param name alias name of the variable
 *
 *  @return alias name of the variable
 */
static inline
char *alias_variable(void *var, char *name, variable_type type, int read_only)
{
    if(variable_count > MAX_VARIABLE_COUNT - 1)
    {
        return NULL;
    }

    aliased_variable_t *node = &variable_pool[variable_count++];

    node->ptr  = var;
    node->type = type;
    node->read_only = read_only;

    stringHashMapInsert(variable_map, name, (void *)node);

    return name;
}

/**
 *  extract the pointer to the variable
 *
 *  @param name alias name of the variable
 *
 *  @return pointer to the variable, NULL if does not exist
 */
static inline
void *extract_variable(char *name)
{
    StringHashMapNode *result = stringHashMapFind(variable_map, name);

    return result ? result->value : NULL;
}

/**
 *  access a variable with a given alias name
 *
 *  @param _name alias name of the variable
 *  @param _type type of the variable
 *
 *  @return pointer to the variable, NULL if does not exist
 */
#define VARIABLE(_name, _type) (*(_type *)(((aliased_variable_t *)extract_variable(_name))->ptr))

#define MODBUS_HANDLERS_MAX			255

/* ModBus-RTU function codes */
#define MODBUS_FUNC_MOTION_GO 		0x90
#define MODBUS_FUNC_MOTION_TRIGGER	0x99

typedef int modbus_cmd_handler_func(motor_control_session_t *, uint8_t *data);

/**
 *  clear handlers and install new handlers
 */
void modbus_handlers_init(void);

/**
 *  register a ModBus function handler
 *
 *  @param func_code 	ModBus-RTU function code
 *  @param handler 		pointer to handler
 */
void modbus_handler_register(uint8_t func_code, modbus_cmd_handler_func handler);

/**
 *  extract variable values from a ModBus-RTU message
 *
 *  @param data 	ModBus-RTU message
 *  @param arg_fmt 	argument format
 *
 *  @return pointer to the input data
 */
uint8_t *modbus_extract_args(uint8_t *data, char *arg_fmt, ...);

#endif /* __COMMAND_H_ */

