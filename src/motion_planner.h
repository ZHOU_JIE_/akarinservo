#ifndef MOTION_PLANNER_H_
#define MOTION_PLANNER_H_

#include <stddef.h>
#include <stdint.h>

typedef struct
{
    float current_pos;
    float target_pos;

    int state;
    int mc_tick;

    float move_amount;

    float acc_peak;
    float vel_peak;

    float vel_demand;
    float acc_demand;
    float pos_demand;

    float pos_output;

    int ramp_duration;
    int direction;
    int const_acc_section;

} motion_controller_t;


motion_controller_t *motion_control_tick(motion_controller_t *ctrl);

motion_controller_t *motion_prepare(motion_controller_t *c,
                                    float current_pos, float target_pos,
                                    float acc_peak, float vel_peak);

#endif /* MOTION_PLANNER_H_ */
