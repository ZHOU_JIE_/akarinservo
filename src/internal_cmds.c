/*
 * Copyright (c) 2015, 2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "hw/sharc_hw.h"
#include "motor_control.h"
#include <inttypes.h>
#include <stdint.h>
#include <stdarg.h>

#include "kalman/kalman_filter.h"
#include "hw/encoder.h"
#include "command/command.h"

COMMAND_HANDLER(zero)
{
    if(argc != 0) return 1;

    session->x_offset =
    		angular_pos_difference(angular_pos_from_float(0.0f, PERIOD_REVOLUTION),
    				session->rot_pos_set, PERIOD_REVOLUTION
			);

    uputs("accepted");

    return 0;
}

COMMAND_HANDLER(set_ctrl_state)
{
    if(argc != 1)
    {
        uprintf("usage: set_ctrl_state state");
        return 1;
    }

    session->state = atoi(argv[0]);
    uputs("accepted");

    return 0;
}

extern int motion_trigger;
extern float home_vel;
extern int home_found;

void motion_cmd_go(motor_control_session_t *session, float target_pos, float target_acc, float target_vel)
{
	int64_t target_pos_count = target_pos / session->controller_configs.reference_unit * POS_PER_REVOLUTION;

    motion_t motion_go = motion_make(1, target_pos_count, target_acc, target_vel);
    motion_go.motion_type = MOTION_GO;
    motion_enqueue(&motion_queue, motion_go);
}

void motion_cmd_trigger(motor_control_session_t *session)
{
	 motion_trigger = 1;
}

void motion_cmd_zero(motor_control_session_t *session)
{
	home_vel = 0.0f;
	home_found = 0;

    motion_t motion_home = motion_make(1, 0, 0, 0);
    motion_home.duration = 0;
    motion_home.motion_type = MOTION_ZERO;
    motion_enqueue(&motion_queue, motion_home);
}

void motion_cmd_home(motor_control_session_t *session)
{
	home_vel = 0.0f;
	home_found = 0;

    motion_t motion_home = motion_make(1, 0, 0, 0);
    motion_home.duration = 0;
    motion_home.motion_type = MOTION_HOME;
    motion_enqueue(&motion_queue, motion_home);
}

void motion_cmd_pause(motor_control_session_t *session, int duration)
{
    motion_t motion_pause = motion_make(1, 0, 0, 0);
    motion_pause.duration = duration;
    motion_pause.motion_type = MOTION_PAUSE;
    motion_enqueue(&motion_queue, motion_pause);
}

COMMAND_HANDLER(motion)
{
    if(argc == 0)
    {
        uputs("usage: motion g <pos> [acc] [tv]\r\n");
        uputs("usage: motion p <ticks>");

        return 1;
    }

    if(strlen(argv[0]) != 1)
    {
        uputs("motion type length error");

        return 1;
    }

    char motion_type = argv[0][0];

    switch(argv[0][0])
    {
        case 'g':
        {
            /* get the position demand and offset */
            //int64_t pos = strtoll(argv[1], NULL, 10) - session->x_offset;
        	float pos = atof(argv[1]);

            //int64_t target_pos = pos / session->controller_configs.reference_unit * POS_PER_REVOLUTION;

            //int64_t initial_pos = session->rot_pos_meas;
            float target_acc =  0.002;
            float target_vel =  session->encoder_configs.rated_rpm / 73.2421875f; //26;

            /* get acceleration demand */
            if(argc > 2)
            {
                target_acc = atof(argv[2]);
            }

            /* get velocity demand */
            if(argc > 3)
            {
                target_vel = atof(argv[3]);
            }

            motion_cmd_go(&mc_session, pos, target_acc, target_vel);

            break;
        }

        case 'p':
        {
        	motion_cmd_pause(&mc_session, atoi(argv[1]));
            break;
        }

        case 'h':
        {
        	motion_cmd_home(&mc_session);
        	break;
        }

        case 'z':
        {
        	motion_cmd_zero(&mc_session);
        	break;
        }

        case 't':
        	motion_cmd_trigger(&mc_session);
        	break;
    	}

    	uputs("accepted");

    return 0;
}

COMMAND_HANDLER(get_pid_pos)
{
    if(argc != 0) return 1;

    uprintf("pid:pos:%f,%f,%f",
            session->position_controller.p,
            session->position_controller.i,
            session->position_controller.d);

    return 0;
}

COMMAND_HANDLER(set_pid_pos)
{
    if(argc != 3)
    {
        uprintf("usage: set_pid_pos p, i, d");
        return 1;
    }

    session->position_controller.p = atof(argv[0]);
    session->position_controller.i = atof(argv[1]);
    session->position_controller.d = atof(argv[2]);

    session->position_controller.integral = 0.0f;

    uputs("accepted");

    return 0;
}

COMMAND_HANDLER(set_pid_torque)
{
    if(argc != 3)
    {
        uprintf("usage: set_pid_torque p, i, d");
        return 1;
    }

    session->torque_controller.p = atof(argv[0]);
    session->torque_controller.i = atof(argv[1]);
    session->torque_controller.d = atof(argv[2]);

    session->torque_controller.integral = 0.0f;

    uputs("accepted");

    return 0;
}

COMMAND_HANDLER(get_pid_torque)
{
    if(argc != 0) return 1;

    uprintf("pid:torque:%f,%f,%f",
            session->torque_controller.p,
            session->torque_controller.i,
            session->torque_controller.d);

    return 0;
}

COMMAND_HANDLER(set_pid_flux)
{
    if(argc != 3)
    {
        uprintf("usage: set_pid_flux p, i, d");
        return 1;
    }

    session->mag_flux_controller.p = atof(argv[0]);
    session->mag_flux_controller.i = atof(argv[1]);
    session->mag_flux_controller.d = atof(argv[2]);

    session->mag_flux_controller.integral = 0.0f;

    uputs("accepted");

    return 0;
}

COMMAND_HANDLER(get_pid_flux)
{
    if(argc != 0) return 1;

    uprintf("pid:flux:%f,%f,%f",
            session->mag_flux_controller.p,
            session->mag_flux_controller.i,
            session->mag_flux_controller.d);

    return 0;
}

COMMAND_HANDLER(get_pid_vel)
{
    if(argc != 0) return 1;

    uprintf("pid:vel:%f,%f,%f",
            session->velocity_controller.p,
            session->velocity_controller.i,
            session->velocity_controller.d);

    return 0;
}

COMMAND_HANDLER(get_name)
{
	if(argc != 0) return 1;

	uprintf("%s", mc_session.controller_configs.name);

	return 0;
}

COMMAND_HANDLER(set_name)
{
	if(argc != 1) return 1;

	strncpy(mc_session.controller_configs.name, argv[0], 64);

	uprintf("accepted");

	return 0;
}

COMMAND_HANDLER(set_pid_vel)
{
    if(argc != 3)
    {
        uprintf("usage: set_pid_vel p, i, d");
        return 1;
    }

    session->velocity_controller.p = atof(argv[0]);
    session->velocity_controller.i = atof(argv[1]);
    session->velocity_controller.d = atof(argv[2]);

    session->velocity_controller.integral = 0.0f;

    uputs("accepted");

    return 0;
}

COMMAND_HANDLER(var_list)
{
#define REPORT_VARIABLE(name)               \
    do {                                    \
        if(strcmp(name, "dummy") != 0) {    \
            uputs(name);                    \
            uputs(",");                     \
        }                                   \
    } while(0)

    if(argc != 0)
    {
        uputs("usage: var_list");

        return 1;
    }

    for(int i = 0; i < variable_map->hashArraySize; i++)
    {
        StringHashMapNode *current = &variable_map->root[i];


        if(current->next)
        {
            REPORT_VARIABLE(current->key);
        }

        while (current)
        {
            current = current->next;
            if(current)
            {
                if(current->next)
                {
                    REPORT_VARIABLE(current->key);
                }
            }
        }

        uputs("\r\n");
    }

    return 0;
}

COMMAND_HANDLER(var_set)
{
    /* var_set var value [type] */

    if(argc != 2 && argc !=3)
    {
        uprintf("usage: var_set var, value, [+|-]");

        return 1;
    }

    aliased_variable_t *var = (aliased_variable_t *)extract_variable(argv[0]);

    if(!var)
    {
        uprintf("error: var %s not found", argv[0]);
        return 2;
    }

    if(var->read_only)
    {
        uprintf("error: var %s is read only", argv[0]);
        return 3;
    }

#define OP_PLUS   1
#define OP_MINUS -1

    int op = 0;

    if(argc == 3)
    {
        if(strlen(argv[2]) != 1)
        {
            uprintf("[%s]\r\n", argv[2]);
            uprintf("error while parsing operation, len: %d", strlen(argv[2]));
            return 4;
        }

        if(argv[2][0] == '+')
        {
            op = OP_PLUS;
        }
        else if(argv[2][0] == '-')
        {
            op = OP_MINUS;
        }
        else
        {
            uprintf("operation not recognized, expecting '+' or '-'");
            return 4;
        }
    }

    switch(var->type)
    {
        case VAR_INT32:
            if(op != 0)
            {
                VARIABLE(argv[0], int) += atoi(argv[1]) * op;
            }
            else
            {
                VARIABLE(argv[0], int) = atoi(argv[1]);
            }
            break;
        case VAR_FLOAT32:
            if(op != 0)
            {
                VARIABLE(argv[0], float) += atof(argv[1]) * op;
            }
            else
            {
                VARIABLE(argv[0], float) = atof(argv[1]);
            }
            break;
        case VAR_INT64:
            if(op != 0)
            {
                VARIABLE(argv[0], int64_t) += strtoll(argv[1], NULL, 10) * op;
            }
            else
            {
                VARIABLE(argv[0], int64_t) = strtoll(argv[1], NULL, 10);
            }
            break;

        case VAR_ANGULAR_POS:
        	if(op != 0)
        	{
        		VARIABLE(argv[0], angular_pos_t) = angular_pos_from_float(atof(argv[1]) * op, PERIOD_REVOLUTION);
        	}
        	else
        	{
        		VARIABLE(argv[0], angular_pos_t) = angular_pos_from_float(atof(argv[1]), PERIOD_REVOLUTION);
        	}
        	break;

        default:
            uprintf("variable type not recognized");
    }

    uputs("accepted");
    return 0;
}

COMMAND_HANDLER(var_get)
{
    if(argc != 1)
    {
        uprintf("usage: var_get var");

        return 1;
    }

    aliased_variable_t *var = (aliased_variable_t *)extract_variable(argv[0]);

    if(!var)
    {
        uprintf("var %s not found", argv[0]);
        return 2;
    }

    switch(var->type)
    {
        case VAR_INT32:
            uprintf("%s:%d", argv[0], VARIABLE(argv[0], int));
            break;
        case VAR_FLOAT32:
            uprintf("%s:%g", argv[0], VARIABLE(argv[0], float));
            break;
        case VAR_INT64:
            uprintf("%s:%lld", argv[0], VARIABLE(argv[0], int64_t));
            break;
        case VAR_ANGULAR_POS:
        	uprintf("%s:%g", argv[0], angular_pos_to_float(VARIABLE(argv[0], angular_pos_t), PERIOD_REVOLUTION));
        	break;

        default:
            uprintf("variable type not recognized");
    }

    return 0;
}

COMMAND_HANDLER(var_get_prop)
{
    if(argc != 1)
    {
        uprintf("usage: var_get_prop var");

        return 1;
    }

    aliased_variable_t *var = (aliased_variable_t *)extract_variable(argv[0]);

    if(!var)
    {
        uprintf("var %s not found", argv[0]);
        return 2;
    }

    uputs(argv[0]);
    uputs(":");

    switch(var->type)
    {
        case VAR_INT32:
            uputs("int32");
            break;
        case VAR_FLOAT32:
            uputs("float32");
            break;
        case VAR_INT64:
            uputs("int64");
            break;
        case VAR_ANGULAR_POS:
        	uputs("angular_pos");
        	break;

        default:
            uprintf("???");
    }

    uprintf(",%d\r\n", var->read_only);

    return 0;
}

COMMAND_HANDLER(ver)
{
    if(argc != 0) return 1;
    uputs("AKARIN Servo build " __DATE__ ", " __TIME__);

    return 0;
}

COMMAND_HANDLER(ping)
{
    if(argc != 0) return 1;
    uputs("pong");

    return 0;
}

COMMAND_HANDLER(set_mod_freq)
{
    if(argc != 1)
    {
        uprintf("usage: set_mod_freq <freq>");

        return 1;
    }

    int mod_freq = atoi(argv[0]);

    if(mod_freq > 30000 || mod_freq < 5000)
    {
        uprintf("error: frequency out of range(%dHz).", mod_freq);

        return 2;
    }

    set_mod_frequency(mod_freq);

    uputs("accepted");
    return 0;
}

void print_variable_record(float *buffer, int len);

COMMAND_HANDLER(scope) //(void *a, int argc, char *argv[])
{
    if(argc != 4)
    {
        uprintf("usage: scope <cmds> <var_name> <length>");
        return 1;
    }

    //printf("exec: [%s]\r\n", argv[0]);

    /* extract pre-commands */
    char *ptr = argv[0];
    char *cmd_head = ptr;

    for(; *ptr; ptr++)
    {
        if(*ptr == '!')
        {
            *ptr = '\0';

            /* remove leading spaces */
            while(*cmd_head && *cmd_head == ' ') cmd_head++;

            char cmd_line[32] = { 0 };
            strncpy(cmd_line, cmd_head, 32);
            cmd_head = ptr + 1;

            int ret = execute_command(session, command_list, cmd_line);

            if(ret != 0)
            {
                uprintf("\r\nerror while executing command: %s, return value = %d\r\n", cmd_line, ret);
            }
        }
    }

    scope_session.var_scope_var1 = extract_variable(argv[1]);

    if(strcmp(argv[2], "null") == 0)
    {
    	scope_session.var_scope_var2 = NULL;
        strncpy(scope_session.var_scope_var2_name, "null", 32);
    }
    else
    {
    	scope_session.var_scope_var2 = extract_variable(argv[2]);
        strncpy(scope_session.var_scope_var2_name, argv[2], 32);
    }

    int record_length = atoi(argv[3]);

    if(record_length <= 0 || record_length > 2048)
    {
        uprintf("invalid value for <length>");
        return 1;
    }

    if(!scope_session.var_scope_var1)
    {
        uprintf("error: no such variable");
        return 1;
    }

    strncpy(scope_session.var_scope_var1_name, argv[1], 32);

    //session->rotor_pos_set = 8000;

    scope_session.var_scope_triggered = 0;
    scope_session.var_scope_trigger_count = record_length;
    scope_session.var_scope_sample_len = 0;

    // -----------------

    while(scope_session.var_scope_triggered == 0)
    {
        /* let the core timer interrupt do its job */
        idle();
    }


    int recorded_length = scope_session.var_scope_trigger_count;

    scope_session.var_scope_triggered = 0;
    scope_session.var_scope_sample_len = 0;
    scope_session.var_scope_trigger_count = 0;
    scope_session.count_div = 0;

    if(scope_session.var_scope_var2 != NULL)
        uprintf("=var_log$%d$[%s-%s]~",
        		recorded_length,
				scope_session.var_scope_var1_name,
				scope_session.var_scope_var2_name);
    else
        uprintf("=var_log$%d$[%s]~", recorded_length, scope_session.var_scope_var1_name);

    print_variable_record(scope_session.var1_scope_buffer, recorded_length);

    if(scope_session.var_scope_var2 != NULL)
    {
        print_variable_record(scope_session.var2_scope_buffer, recorded_length);
    }

    uputs("$log;");

    scope_session.var_scope_triggered = 0;

    return 0;
}


COMMAND_HANDLER(write_encoder_header)
{
    encoder_eeprom_write_byte(0x00, 0x00);
    encoder_eeprom_write_byte(0x01, 0x93);
    encoder_eeprom_write_byte(0x02, 0x00);
    encoder_eeprom_write_byte(0x03, 0x93);

    if(encoder_eeprom_read_byte(0x00) != 0x00 ||
       encoder_eeprom_read_byte(0x01) != 0x93 ||
       encoder_eeprom_read_byte(0x02) != 0x00 ||
       encoder_eeprom_read_byte(0x03) != 0x93)
    {
        uprintf("encoder header verify failed");

        return 1;
    }

    uprintf("encoder header written");

    return 0;
}

COMMAND_HANDLER(notch_filter_tune)
{
	if(argc != 3)
	{
		uputs("usage: nfilter_tune <n> <cf> <bw>");
		return 1;
	}

	int filter_index = atoi(argv[0]);
	int enable = atoi(argv[3]);

	/* convert frequency into fraction of sample rate */
	float center_freq = atof(argv[1]) / (float)CONTROL_FREQUENCY;
	float bandwidth = atof(argv[2]) / (float)CONTROL_FREQUENCY;

	if(filter_index > SESSION_IIR_FILTER_COUNT - 1)
	{
		uputs("filter index out of bound");
		return 2;
	}

	/* get filter pointer */
	iir_filter_t *filter = &session->notch_filters[filter_index];

	/* run IIR design algorithm */
	calculate_iir_coeff(
			bandwidth, center_freq,
			&filter->a0, &filter->a1, &filter->a2,
			&filter->b1, &filter->b2
	);

	/* copy filter info */
	filter->cf = center_freq;
	filter->bw = bandwidth;

	uputs("accepted");

	return 0;
}

COMMAND_HANDLER(notch_filter_enable)
{
	if(argc != 2)
	{
		uputs("usage: nfilter <n> <enable>");
		return 1;
	}

	int filter_index = atoi(argv[0]);
	int enable = atoi(argv[1]);

	if(enable != 0 && enable != 1)
	{
		uputs("unexpected value for <enable>");
		return 1;
	}

	if(filter_index > SESSION_IIR_FILTER_COUNT - 1)
	{
		uputs("filter index out of bound");
		return 2;
	}

	if(enable)
	{
		session->enabled_iir_notch_filters |= (1 << filter_index);
	}
	else
	{
		session->enabled_iir_notch_filters &= ~(1 << filter_index);
	}

	uputs("accepted");

	return 0;
}

COMMAND_HANDLER(nfilter_info)
{
	if(argc != 1)
	{
		uputs("usage: nfilter_info <n>");
		return 1;
	}

	int filter_index = atoi(argv[0]);

	if(filter_index > SESSION_IIR_FILTER_COUNT - 1)
	{
		uputs("filter index out of bound");
		return 2;
	}

	/* print filter info
	 * filter n:cf,bw,enabled */
	iir_filter_t *filter = &session->notch_filters[filter_index];

	uprintf("filter %d:%g,%g,%d", filter_index, filter->cf, filter->bw,
			(session->enabled_iir_notch_filters & (1 << filter_index)) > 0);

	return 0;
}

COMMAND_HANDLER(nfilter_list)
{
	if(argc != 0)
	{
		uputs("usage: nfilter_list");
		return 1;
	}

	return 0;
}

COMMAND_HANDLER(get_error_what)
{
	if(argc != 1)
	{
		uputs("usage: get_error_what <id>");
		return 1;
	}

	int id = atoi(argv[0]);

	for(int i = 0; i < sizeof(_errors) / sizeof(motor_control_error); i++)
	{
		if(id == _errors[i].id)
		{
			uprintf("%d:%s", id, _errors[i].message);
			return 0;
		}
	}

	uprintf("%d:not found", id);

	return 2;
}

int initialize_commands(void)
{
    stringHashMapInsert(command_list, "zero", COMMAND(zero));
    stringHashMapInsert(command_list, "set_ctrl_state", COMMAND(set_ctrl_state));

    stringHashMapInsert(command_list, "set_pid_pos", COMMAND(set_pid_pos));
    stringHashMapInsert(command_list, "get_pid_pos", COMMAND(get_pid_pos));

    stringHashMapInsert(command_list, "set_pid_torque", COMMAND(set_pid_torque));
    stringHashMapInsert(command_list, "get_pid_torque", COMMAND(get_pid_torque));

    stringHashMapInsert(command_list, "set_pid_vel", COMMAND(set_pid_vel));
    stringHashMapInsert(command_list, "get_pid_vel", COMMAND(get_pid_vel));

    stringHashMapInsert(command_list, "set_pid_flux", COMMAND(set_pid_flux));
    stringHashMapInsert(command_list, "get_pid_flux", COMMAND(get_pid_flux));

    /* variable map commands */
    stringHashMapInsert(command_list, "var_get", COMMAND(var_get));
    stringHashMapInsert(command_list, "get",     COMMAND(var_get));
    stringHashMapInsert(command_list, "var_set", COMMAND(var_set));
    stringHashMapInsert(command_list, "set",     COMMAND(var_set));
    stringHashMapInsert(command_list, "var_list", COMMAND(var_list));
    stringHashMapInsert(command_list, "var_get_prop", COMMAND(var_get_prop));

    /* misc */
    stringHashMapInsert(command_list, "ver", COMMAND(ver));
    stringHashMapInsert(command_list, "ping", COMMAND(ping));

    stringHashMapInsert(command_list, "set_mod_freq", COMMAND(set_mod_freq));

    stringHashMapInsert(command_list, "scope", COMMAND(scope));

    stringHashMapInsert(command_list, "write_enc_hdr", COMMAND(write_encoder_header));

    stringHashMapInsert(command_list, "motion", COMMAND(motion));
    stringHashMapInsert(command_list, "m", COMMAND(motion));

    stringHashMapInsert(command_list, "nfilter_tune", COMMAND(notch_filter_tune));
    stringHashMapInsert(command_list, "nfilter_enable", COMMAND(notch_filter_enable));
    stringHashMapInsert(command_list, "nfilter_info", COMMAND(nfilter_info));
    stringHashMapInsert(command_list, "nfilter_list", COMMAND(nfilter_list));

    stringHashMapInsert(command_list, "get_error_what", COMMAND(get_error_what));

    stringHashMapInsert(command_list, "get_name", COMMAND(get_name));
    stringHashMapInsert(command_list, "set_name", COMMAND(set_name));

    return 0;
}
