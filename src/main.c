/*
 * Copyright (c) 2015, 2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "hw/sharc_hw.h"
#include "motor_control.h"
#include <inttypes.h>
#include <stdint.h>

#include "kalman/kalman_filter.h"
#include "hw/encoder.h"
#include "self_test/self_test.h"

#define ABS(x) ((x) > 0 ? (x) : -(x))

uint32_t global_error = 0;

void motor_control_initialize_components(void);

void kalman_filter_setup(motor_control_session_t *session)
{
    float dt = 0.5;

    /* prediction matrix */
    INSMatrix33f A = {
        .m11 = 1.0, .m12 = -(dt * dt) / 2.0,    .m13 = 0.0,
        .m21 = 0.0, .m22 = 1.0,                 .m23 = -(dt * dt) / 2.0,
        .m31 = 0.0, .m32 = 0.0,                 .m33 = 1.0
    };

    /* covariance of the error noise */
    INSMatrix33f Q = {
        0.05, 0.0, 0.0,
        0.0, 0.0005, 0.0,
        0.0, 0.0, 0.00001
    };

    /* noise in the sensor measurement */
    INSMatrix33f R = {
        300.0, 0.0, 0.0,
        0.0, 800.0, 0.0,
        0.0, 0.0, 1000.0
    };

    /* measurement matrix */
    INSMatrix33f H = {
        .m11 = 1.0, .m12 = 0.0, .m13 = 0.0,
        .m21 = 0.0, .m22 = 1.0, .m23 = 0.0,
        .m31 = 0.0, .m32 = 0.0, .m33 = 1.0
    };

    /* control matrix */
    INSMatrix33f B = M33F_IDENTITY;

    insKalmanFilterInit(&session->angular_vel_kalman_filter, A, Q, R, B, H);
}

void matrix33_print(INSMatrix33f *m)
{
    uprintf("[%f,\t%f,\t%f,\r\n", m->m11, m->m12, m->m13);
    uprintf(" %f,\t%f,\t%f,\r\n", m->m21, m->m22, m->m23);
    uprintf(" %f,\t%f,\t%f]\r\n", m->m31, m->m32, m->m33);
}

void kalman_print(INSKalmanFilter *kalman)
{
    uputs("A:\r\n");
    matrix33_print(&kalman->A);
    uputs("B:\r\n");
    matrix33_print(&kalman->B);
    uputs("Q:\r\n");
    matrix33_print(&kalman->Q);
    uputs("R:\r\n");
    matrix33_print(&kalman->R);
    uputs("H:\r\n");
    matrix33_print(&kalman->H);
}

#define ABS2(x) (((x) > 0) ? (x) : -(x))

#include "command/command.h"

COMMAND_HANDLER(kalman_print)
{
    if(argc != 0) return 1;

    uputs("kalman:");
    kalman_print(&session->angular_vel_kalman_filter);

    return 0;
}

COMMAND_HANDLER(set_torque_ff)
{
    if(argc != 1) return 1;

    session->encoder_configs.torque_feedforward = atof(argv[0]);
    uputs("accepted");
    return 0;
}

void dai_spi_setup(void)
{
    //SRU(DPI_PB01_O, SPIB_MISO_I);
    //SRU(SPIB_MISO_PBEN_O, DPI_PBEN01_I);
    SRU(DPI_PB01_O, SPIB_MISO_I);     //Connect DPI PB2 to MISO.
    SRU(SPIB_MISO_PBEN_O, DPI_PBEN01_I);
    SRU(LOW, DAI_PBEN09_I);

    /* setup SPI signals */
    SRU(SPIB_MOSI_O, DPI_PB05_I);
    SRU(SPIB_MOSI_PBEN_O, DPI_PBEN05_I);
    SRU(LOW, DAI_PBEN20_I);

    SRU(SPIB_CLK_O, DPI_PB04_I);
    SRU(SPIB_CLK_PBEN_O, DPI_PBEN04_I);
    SRU(LOW, DAI_PBEN04_I);


        *pSPIDMACB = 0;
        *pSPIBAUDB = 0;
        *pSPIFLGB = 0x8F00;//0xF80;

        *pSPICTLB = 0;

        *pSPIBAUDB = 100;
        *pSPIDMACB = 0;
        *pIISPIB = 0;
        *pIMSPIB = 0;
}

void dsp_gpio_setup(void)
{
    // LCD
    SRU(HIGH, DAI_PBEN12_I);
    SRU(HIGH, DAI_PBEN16_I);

    // RS485
    SRU(HIGH, DAI_PBEN02_I);

    // LED
    SRU(HIGH, DAI_PBEN18_I);
    SRU(HIGH, DAI_PBEN17_I);
    SRU(HIGH, DAI_PBEN11_I);

    // EEPROM CS
    SRU(HIGH, DAI_PBEN10_I);
    // ADC CS
    SRU(HIGH, DAI_PBEN14_I);

    EEPROM_CS_HIGH();
    ADC_CS_HIGH();

    // PB08 = ADC BUSY
    SRU(HIGH, DAI_PBEN08_I);

    SRU(HIGH, DAI_PBEN07_I);

    // P10 = EEPROM CS
    SRU(HIGH, DAI_PBEN10_I);

    // P20 = controller EEPROM CS
    SRU(HIGH, DAI_PBEN20_I);
}

void motor_control_initialize_components(void)
{
    for(int i = 0; i < 100; i++)
    {
        motor_control_read_encoder();
    }

    mc_session.state = CONTROL_VELOCITY;
    mc_session.encoder_count = motor_control_read_encoder();
    mc_session.rot_pos_set = angular_pos_from_float(motor_control_read_encoder(), PERIOD_REVOLUTION);
    mc_session.mechanical_angle_prev = angular_pos_to_float(mc_session.rot_pos_set, PERIOD_REVOLUTION);
    mc_session.rot_pos_meas = mc_session.rot_pos_set;
    mc_session.rot_vel_meas = 0;
    mc_session.rot_vel_set = 0;

    for(int i = 0; i < 1000; i++)
    {
        ab_observer_update(&mc_session.vel_ff_observer, (float)angular_pos_to_float(mc_session.rot_pos_set, PERIOD_REVOLUTION));
        ab_observer_update(&mc_session.torque_ff_observer, mc_session.vel_ff_observer.vk_1);

        mc_session.rot_vel_meas = iir_filter_update(&mc_session.vel_filter, mc_session.rot_vel_meas);
    }

    for(int i = 0; i < 1000; i++)
    {
    	motor_control_read_sensors(&mc_session);
    }

    mc_session.encoder_misscode_count = 0;
    mc_session.encoder_check_enabled = 1;
}

static void akarin_print_pid_settings(pid_settings_t *pid)
{
    uprintf("%.6f, %.6f, %.6f\r\n", pid->p, pid->i, pid->d);
}

void akarin_encoder_print(akarin_encoder_t *encoder)
{
    uprintf("AKARIN ENCODER INFO\r\n");
    uprintf("    ID: %d\r\n", encoder->id);
    uprintf("    Motor poles: %d\r\n", encoder->poles);
    uprintf("    Motor encoder offset: %f\r\n", encoder->encoder_offset);

    uprintf("    Position PID: ");
    akarin_print_pid_settings(&encoder->pid_position);
    uprintf("    Velocity PID: ");
    akarin_print_pid_settings(&encoder->pid_velocity);
    uprintf("    Torque PID  : ");
    akarin_print_pid_settings(&encoder->pid_torque);
    uprintf("    Mag Flux PID: ");
    akarin_print_pid_settings(&encoder->pid_flux);

    uprintf("    Velocity feed-forward: %f\r\n", encoder->vel_feedforward);
    uprintf("    Torque   feed-forward: %f\r\n", encoder->torque_feedforward);
    uprintf("    Torque coeff: %f NM/A\r\n", encoder->torque_coeff);

    uprintf("    Rated RPM: %f\r\n", encoder->rated_rpm);
    uprintf("    Torque limit: %f\r\n", encoder->torque_limit);
    uprintf("    Current limit: %f\r\n", encoder->motor_current_protect_limit);
    uprintf("    Rotor Velocity Limit: %.1f Deg/s\r\n", encoder->rotor_vel_limit);
}

static void encoder_restore(motor_control_session_t *session)
{
    dsp_core_timer_disable();

    akarin_encoder_t *encoder = &mc_session.encoder_configs;
    encoder_eeprom_restore(encoder, 16);

    pid_controller_update_settings(&session->position_controller, &encoder->pid_position);
    pid_controller_update_settings(&session->velocity_controller, &encoder->pid_velocity);
    pid_controller_update_settings(&session->torque_controller,   &encoder->pid_torque);
    pid_controller_update_settings(&session->mag_flux_controller, &encoder->pid_flux);

    pid_controller_reset(&session->position_controller);
    pid_controller_reset(&session->velocity_controller);
    pid_controller_reset(&session->torque_controller);
    pid_controller_reset(&session->mag_flux_controller);

    akarin_encoder_print(encoder);
    dsp_core_timer_enable();
}

COMMAND_HANDLER(encoder_restore)
{
    if(argc != 0) return 1;

    encoder_restore(session);

    uputs("accepted");

    return 0;
}

COMMAND_HANDLER(encoder_save)
{
    if(argc != 0) return 1;

    dsp_core_timer_disable();

    akarin_encoder_t *encoder = &mc_session.encoder_configs;

    pid_controller_extract_settings(&session->position_controller, &encoder->pid_position);
    pid_controller_extract_settings(&session->velocity_controller, &encoder->pid_velocity);
    pid_controller_extract_settings(&session->torque_controller,   &encoder->pid_torque);
    pid_controller_extract_settings(&session->mag_flux_controller, &encoder->pid_flux);

    encoder_eeprom_save(encoder, 16);
    akarin_encoder_t encoder2;
    encoder_eeprom_restore(&encoder2, 16);

    akarin_encoder_print(&encoder2);

    uputs("accepted");
    dsp_core_timer_enable();

    return 0;
}

COMMAND_HANDLER(ctrl_eeprom_save)
{
    if(argc != 0) return 1;

    dsp_core_timer_disable();

    motor_controller_config_t *config = &session->controller_configs;

    session->controller_configs.crc32 = crc32(0, config, sizeof(motor_controller_config_t) - sizeof(uint32_t));
    controller_eeprom_save(config, sizeof(motor_controller_config_t), 16);

    uputs("accepted");
    dsp_core_timer_enable();

    return 0;
}

void ctrl_eeprom_restore(motor_control_session_t *session)
{
    dsp_core_timer_disable();

    /* get the pointer to the session config */
    motor_controller_config_t *config = &session->controller_configs;

    /* load config from EEPROM */
    controller_eeprom_restore(config, sizeof(motor_controller_config_t), 16);

    /* calculate checksum, not including the last CRC word */
    uint32_t crc_cksm = crc32(0, config,
            sizeof(motor_controller_config_t) - sizeof(uint32_t)
    );

    if(crc_cksm != config->crc32)
    {
        SET_ERROR(ERROR_CTRL_EEPROM_CKSM);
    }

    dsp_core_timer_enable();

    uprintf("load: %d %d %f %f\r\nlocal addr: %d\r\n",
            config->hall_a_bias, config->hall_b_bias, config->hall_a_coeff, config->hall_b_coeff,
            config->local_address);
    uprintf("CRC32 checksum: %d\r\n", config->crc32);
}

COMMAND_HANDLER(ctrl_eeprom_restore)
{
    if(argc != 0) return 1;

    ctrl_eeprom_restore(session);

    return 0;
}

COMMAND_HANDLER(control_resume)
{
	if(argc != 0) return 1;

	dsp_core_timer_disable();
	motor_control_initialize_components();
	dsp_core_timer_enable();

	uprintf("accepted");
	return 0;
}

void command_setup(void)
{
    initialize_commands();

    stringHashMapInsert(command_list, "set_torque_ff", COMMAND(set_torque_ff));

    stringHashMapInsert(command_list, "get_kalman", COMMAND(kalman_print));

    /* encoder commands */
    stringHashMapInsert(command_list, "encoder_save", COMMAND(encoder_save));
    stringHashMapInsert(command_list, "encoder_restore", COMMAND(encoder_restore));

    stringHashMapInsert(command_list, "ctrl_eeprom_save", COMMAND(ctrl_eeprom_save));
    stringHashMapInsert(command_list, "ctrl_eeprom_restore", COMMAND(ctrl_eeprom_restore));
    stringHashMapInsert(command_list, "control_resume", COMMAND(control_resume));
}

// ----------

// ----------

int command_labled = 0;

char *extract_labeled_command(char *cmd_in)
{
    char command_label[32] = { 0 };
    char *ptr = command_label;

    if(*cmd_in == '*')
    {
        while(*cmd_in++ != ' ' && *cmd_in != '\0')
        {
            *ptr++ = *cmd_in;
        }

        command_labled = atoi(command_label);
    }
    else
    {
        command_labled = 0;
    }

    return cmd_in;
}

void process_command(void)
{
	if(uart_rx_idle == 0)
	{
		return;
	}

	uart_rx_idle = 0;

    /* clear state machine states */
    int rx_bytes = uart_rx_count;
    uart_rx_count = 0;

    /* drop the pack if is too long */
    if(rx_bytes >= 255)
    {
        return;
    }

    /* copy command to a new array */
    char command[256];
    memcpy(command, uart_rx_buffer, rx_bytes);
    command[rx_bytes] = '\0';

    /* see if is a legal command string */
    if(rx_bytes < 2)
    {
        /* should have at least 2 bytes */
        return;
    }

    /* *<addr> <cmd> <arg>... */
    /* a legal command string starts with '*' */
    if(command[0] == '*')
    {
        /* for legacy support,
         * the older version uses ';' as a termination char */
        if(command[rx_bytes - 1] == ';')
        {
            command[rx_bytes - 1] = '\0';
        }

        /* parse command label and get new pointer */
        char *command_head = extract_labeled_command(command);

        if(command_labled == mc_session.controller_configs.local_address ||
        		command_labled == -1 ||
				command_labled == -2)
        {
            if(command_labled == -1)
            {
                /* broadcast message, disable echo,
                 * set output FIFO to NULL */
                uart_out_redirect(NULL);
            }

            uprintf("\r\n[%d]=", command_labled);

            LED_RED_LOW();
            int ret = execute_command(&mc_session, command_list, command_head);
            LED_RED_HIGH();

            if(ret != 0)
            {
                if(ret == -1)
                {
                    /* command not found */
                    uputs("???");
                }
                else
                {
                    uprintf("\r\ncommand returns: %d", ret);
                }
            }

            uputs(";");

            /* restore output FIFO */
            if(command_labled == -1)
            {
                uart_out_redirect(&uart_tx_fifo);
            }
        }

    }
}

int main(int argc, char *argv[])
{
    adi_initComponents();

    dsp_pll_setup();
    dsp_uart_setup();
    dsp_pwm_setup();
    dsp_spi_setup();
    dai_spi_setup();
    dsp_gpio_setup();

    initialize_default_values();

    set_mod_frequency(10000);

    kalman_filter_setup(&mc_session);

    interrupt_init();

    command_list = stringHashMapCreate(17);
    variable_map = stringHashMapCreate(17);

    command_setup();
    modbus_handlers_init();

    initialize_variable_alias();
    initialize_filters();

    uart_out_redirect(&uart_tx_fifo);

    // 1/230400 * 14
    set_gp_timer_expire(60763);



    //
    /* init finish */
    /*int self_test = software_components_post();
    if(self_test == 0)
    {
    	printf("self test ok\n");
    }
    else
    {
    	printf("self test failed\n");
    }*/
    //


    // ------
    ADC_CS_HIGH();
    ADC_CONVST_HIGH();

    LED_GREEN_HIGH();
    LED_RED_HIGH();
    LED_BLUE_HIGH();

    EEPROM_CS_HIGH();
    CTRL_EEPROM_CS_HIGH();

    SRU(HIGH, DAI_PB08_I);
    mag_encoder_release();

    RS485_IDLE();

    //
    //SRU(LOW,DAI_PB09_I);
    SRU2(LOW,DAI_PBEN09_I);         /* disable DPI pin10 as input */

    /* load controller configuration from on board EEPROM */
    ctrl_eeprom_restore(&mc_session);

    /* print message */
    uputs("\r\n");
    uputs("AKARIN Servo build " __DATE__ ", " __TIME__);
    uputs("\r\nNICE Servo, CyanNavis.com\r\n");
    uputs("loading encoder information...\r\n");

    int encoder_not_found = 0;

    /* detect if the encoder is installed */
    if(mag_encoder_detect() != 1)
    {
        SET_ERROR(ERROR_ENCDR_MISSING);

        uprintf("Error: cannot connect to encoder\r\n");

        LED_RED_LOW();
        LED_BLUE_HIGH();

        encoder_not_found = 1;

        //while(1);
    }

    /* load encoder configuration from encoder EEPROM */
    encoder_restore(&mc_session);

    /* setup completed */
    uputs("\r\n> ");

    if(!encoder_not_found)
    {
    	motor_control_initialize_components();

        mc_session.state = CONTROL_POSITION;
    }
    else
    {
        core_timer_enable = 0;
    }

    dsp_core_timer_setup();

    while(1)
    {
        uart_process_fifo();
        process_command();

        // -----------------

        if(mc_session.tick_count > 10000)
        {
            LED_GREEN_HIGH();

            if(mc_session.tick_count > 20000)
            {
                mc_session.tick_count = 0;
            }
        }
        else
        {
            LED_GREEN_LOW();
        }
    }


}

