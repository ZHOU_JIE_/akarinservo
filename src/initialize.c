/*
 * Copyright (c) 2015, 2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "hw/sharc_hw.h"
#include "motor_control.h"
#include <inttypes.h>
#include <stdint.h>
#include <stdarg.h>

#include "kalman/kalman_filter.h"
#include "hw/encoder.h"

#include "command/command.h"

extern int motion_type;
extern int motion_pause_length;
extern int motion_pause_count;

extern float tq_ff_coeff, tq_ff_phase;

int initialize_variable_alias(void)
{
    alias_variable(&mc_session.encoder_configs.poles, "poles", VAR_INT32, 0);
    alias_variable(&mc_session.state, "state", VAR_INT32, 0);
    alias_variable(&mc_session.free_run_torque, "free_q", VAR_FLOAT32, 0);



    alias_variable(&tq_ff_phase, "tq_ff_phase", VAR_FLOAT32, 0);


    alias_variable(&debug_enable, "debug", VAR_INT32, 0);
    alias_variable(&echo_enable, "echo", VAR_INT32, 0);
    alias_variable(&mc_session.encoder_configs.encoder_offset, "enc_off", VAR_FLOAT32, 0);
    alias_variable(&mc_session.encoder_configs.rated_rpm, "rated_rpm", VAR_FLOAT32, 0);
    alias_variable(&mc_session.rotor_angular_vel_control, "rot_vel", VAR_FLOAT32, 0);

    alias_variable(&mc_session.rot_pos_set, "rot_pos_set", VAR_ANGULAR_POS, 0);
    alias_variable(&mc_session.rot_pos_meas, "rot_pos_meas", VAR_ANGULAR_POS, 1);

    alias_variable(&mc_session.electric_angle, "elec_angle", VAR_FLOAT32, 1);

    /* hall sensor calibrations */
    alias_variable(&mc_session.controller_configs.hall_a_bias, "hall.a.bias", VAR_INT32, 0);
    alias_variable(&mc_session.controller_configs.hall_b_bias, "hall.b.bias", VAR_INT32, 0);

    alias_variable(&mc_session.hall_a_raw, "hall.a.raw", VAR_FLOAT32, 0);
    alias_variable(&mc_session.hall_b_raw, "hall.b.raw", VAR_FLOAT32, 0);

    alias_variable(&mc_session.controller_configs.hall_a_coeff, "hall.a.coeff", VAR_FLOAT32, 0);
    alias_variable(&mc_session.controller_configs.hall_b_coeff, "hall.b.coeff", VAR_FLOAT32, 0);

    alias_variable(&mc_session.torque_meas, "torque_meas", VAR_FLOAT32, 1);

    alias_variable(&mc_session.position_controller.p, "pid_pos.p", VAR_FLOAT32, 0);
    alias_variable(&mc_session.position_controller.i, "pid_pos.i", VAR_FLOAT32, 0);
    alias_variable(&mc_session.position_controller.d, "pid_pos.d", VAR_FLOAT32, 0);

    alias_variable(&mc_session.velocity_controller.p, "pid_vel.p", VAR_FLOAT32, 0);
    alias_variable(&mc_session.velocity_controller.i, "pid_vel.i", VAR_FLOAT32, 0);
    alias_variable(&mc_session.velocity_controller.d, "pid_vel.d", VAR_FLOAT32, 0);

    alias_variable(&mc_session.torque_controller.p, "pid_torque.p", VAR_FLOAT32, 0);
    alias_variable(&mc_session.torque_controller.i, "pid_torque.i", VAR_FLOAT32, 0);
    alias_variable(&mc_session.torque_controller.d, "pid_torque.d", VAR_FLOAT32, 0);

    alias_variable(&mc_session.mag_flux_controller.p, "pid_flux.p", VAR_FLOAT32, 0);
    alias_variable(&mc_session.mag_flux_controller.i, "pid_flux.i", VAR_FLOAT32, 0);
    alias_variable(&mc_session.mag_flux_controller.d, "pid_flux.d", VAR_FLOAT32, 0);

    // kalman
    alias_variable(&mc_session.angular_vel_kalman_filter.Q.m11, "klm.q.m11", VAR_FLOAT32, 0);
    alias_variable(&mc_session.angular_vel_kalman_filter.Q.m22, "klm.q.m22", VAR_FLOAT32, 0);
    alias_variable(&mc_session.angular_vel_kalman_filter.Q.m33, "klm.q.m33", VAR_FLOAT32, 0);

    alias_variable(&mc_session.angular_vel_kalman_filter.R.m11, "klm.r.m11", VAR_FLOAT32, 0);
    alias_variable(&mc_session.angular_vel_kalman_filter.R.m22, "klm.r.m22", VAR_FLOAT32, 0);
    alias_variable(&mc_session.angular_vel_kalman_filter.R.m33, "klm.r.m33", VAR_FLOAT32, 0);

    //session->rotor_angular_vel_set
    alias_variable(&mc_session.torque_set, "torque_set", VAR_FLOAT32, 1);
    alias_variable(&mc_session.flux_cmd, "flux_cmd", VAR_FLOAT32, 1);
    alias_variable(&mc_session.flux_meas, "flux", VAR_FLOAT32, 1);
    alias_variable(&mc_session.current_cmd, "current_cmd", VAR_FLOAT32, 1);
    alias_variable(&mc_session.rot_vel_meas, "rot_vel_meas", VAR_FLOAT32, 1);
    alias_variable(&mc_session.rot_vel_set, "rot_vel_set", VAR_FLOAT32, 1);

    alias_variable(&mc_session.encoder_configs.vel_feedforward, "vel_ff", VAR_FLOAT32, 0);
    alias_variable(&mc_session.encoder_configs.torque_feedforward, "torque_ff", VAR_FLOAT32, 0);
    alias_variable(&mc_session.encoder_configs.torque_coeff, "torque_coeff", VAR_FLOAT32, 0);

    // observer settings
    alias_variable(&mc_session.vel_ff_observer.a, "vel_ff_a", VAR_FLOAT32, 0);
    alias_variable(&mc_session.vel_ff_observer.b, "vel_ff_b", VAR_FLOAT32, 0);

    alias_variable(&mc_session.encoder_configs.torque_limit, "torque_limit", VAR_FLOAT32, 0);
    alias_variable(&mc_session.encoder_configs.rotor_vel_limit, "rot_vel_limit", VAR_FLOAT32, 0);

    alias_variable(&scope_session.var_scope_div, "scope_div", VAR_INT32, 0);

    alias_variable(&mc_session.stop, "stop", VAR_INT32, 0);

    alias_variable(&mc_session.ang_error, "pos_error", VAR_FLOAT32, 1);
    alias_variable(&mc_session.ang_derror, "pos_derror", VAR_FLOAT32, 1);

    alias_variable(&mc_session.pos_set_dd, "pos_set_dd", VAR_FLOAT32, 0);

    alias_variable(&mc_session.encoder_misscode_count, "encoder_misscode_count", VAR_INT32, 1);

	alias_variable(&tq_ff_coeff, "tq_ff_coeff", VAR_FLOAT32, 0);
    alias_variable(&mc_session.torque_meas_anticogged, "torque_meas_anticogged", VAR_FLOAT32, 0);

    /*extern int motion_type;
extern int motion_pause_length;
extern int motion_pause_count;*/


    alias_variable(&motion_type, "motion_type", VAR_INT32, 0);
    alias_variable(&motion_pause_length, "motion_pause_length", VAR_INT32, 0);
    alias_variable(&motion_pause_count, "motion_pause_count", VAR_INT32, 0);

    alias_variable(&mc_session.ang_vel_display, "ang_vel_display", VAR_FLOAT32, 1);
    alias_variable(&mc_session.torque_display, "torque_display", VAR_FLOAT32, 1);

    // self tuner
    /*    session.pos_pid_tuner.error_pb 			= 200.0f;
    session.pos_pid_tuner.error_change_pb 	= 10.0f;

    session.pos_pid_tuner.kp_pb		= 3.0f;
    session.pos_pid_tuner.ki_pb 	= 0.02f;
    session.pos_pid_tuner.kd_pb 	= 0.0f;

    session.pos_pid_tuner.p_prime_max = 3.0f;
    session.pos_pid_tuner.i_prime_max = 0.1f;*/
    alias_variable(&mc_session.pos_pid_tuner.error_pb, "pos_sf_error_pb", VAR_FLOAT32, 0);
    alias_variable(&mc_session.pos_pid_tuner.error_change_pb, "pos_sf_derror_pb", VAR_FLOAT32, 0);
    alias_variable(&mc_session.pos_pid_tuner.kp_pb, "pos_sf_kp_pb", VAR_FLOAT32, 0);
    alias_variable(&mc_session.pos_pid_tuner.p_prime_max, "pos_sf_p'_max", VAR_FLOAT32, 0);
    alias_variable(&mc_session.pos_pid_tuner.p, "pos_sf_p", VAR_FLOAT32, 0);

    alias_variable(&mc_session.controller_configs.reference_unit, "ref_unit", VAR_FLOAT32, 0);

    alias_variable(&mc_session.controller_configs.out_of_control_thres, "out_of_control_thres", VAR_INT32, 0);

    alias_variable(&mc_session.controller_configs.local_address, "local_address", VAR_INT32, 0);

    alias_variable(&global_error, "error", VAR_INT32, 0);

    alias_variable(&mc_session.dio_home_switch, "home_switch", VAR_INT32, 0);

    /* performance cycle counter variables */
    alias_variable(&mc_session.performance.max, "cycle_max", VAR_INT32, 1);
    alias_variable(&mc_session.performance.min, "cycle_min", VAR_INT32, 1);
    alias_variable(&mc_session.performance.current, "cycle_current", VAR_INT32, 1);

    // ---
    alias_variable(&mc_session.controller_configs.dio_config.dir, "dio_dir", VAR_INT32, 0);
    alias_variable(&mc_session.dio_state.dio_out, "dio_out", VAR_INT32, 0);
    alias_variable(&mc_session.dio_state.dio_in, "dio_in", VAR_INT32, 0);
    alias_variable(&mc_session.controller_configs.dio_config.usage, "dio_usage", VAR_INT32, 0);

    alias_variable(&mc_session.encoder_raw, "encoder_raw", VAR_INT32, 1);

    alias_variable(&mc_session.motor_current_meas_filtered, "motor_current_meas", VAR_FLOAT32, 1);
    alias_variable(&mc_session.encoder_configs.motor_current_protect_limit, "motor_current_protect_limit", VAR_FLOAT32, 0);

    alias_variable(&core_timer_enable, "core_timer_enable", VAR_INT32, 0);

    alias_variable(&mc_session.set_angle, "set_angle", VAR_FLOAT32, 0);

    // VFD stuff
    alias_variable(&mc_session.vfd.torque_ratio, "vfd_torque_ratio", VAR_FLOAT32, 0);
    alias_variable(&mc_session.vfd.electric_angle_inc, "vfd_electric_angle_inc", VAR_FLOAT32, 0);
    alias_variable(&mc_session.vfd.frequency, "vfd_frequency", VAR_FLOAT32, 0);

	return 0;
}

int initialize_default_values(void)
{
    differentiator_angular_init(&mc_session.pos_diff, PERIOD_REVOLUTION);
    differentiator_float_init(&mc_session.ang_error_diff);

    differentiator_float_init(&mc_session.acc_diff);
    differentiator_float_init(&mc_session.jerk_diff);
    differentiator_float_init(&mc_session.vel_error_diff);


    mc_session.state = 4;

    /* motor settings */
    //session.encoder_offset = -11.0;
    mc_session.encoder_configs.encoder_offset = 0;//-353;//-17.892;
    mc_session.encoder_configs.poles       = 0;//4;
    mc_session.rotor_angular_vel_control = 0;
    mc_session.free_run_torque = 0.0;

    /* control settings & limits */
    mc_session.encoder_configs.rotor_vel_limit = 30000.0f;
    mc_session.encoder_configs.torque_limit      = 40.0f;
    mc_session.encoder_configs.torque_feedforward     = 0.0f;

    mc_session.rot_pos_set = angular_pos_from_float(0.0f, PERIOD_REVOLUTION);
    mc_session.mechanical_angle_prev = 0;

    mc_session.x_offset = angular_pos_from_float(0.0f, PERIOD_REVOLUTION);

    // self tuner
    mc_session.pos_pid_tuner.error_pb 			= 40.0f;
    mc_session.pos_pid_tuner.error_change_pb 	= 10.0f;

    mc_session.pos_pid_tuner.kp_pb		= 5.0f;
    mc_session.pos_pid_tuner.ki_pb 	= 0.02f;
    mc_session.pos_pid_tuner.kd_pb 	= 0.0f;

    mc_session.pos_pid_tuner.p_prime_max = 5.0f;
    mc_session.pos_pid_tuner.i_prime_max = 0.1f;

    // vel self tuner
    mc_session.vel_pid_tuner.error_pb = 200.0f;
    mc_session.vel_pid_tuner.error_change_pb = 20.0f;

    mc_session.vel_pid_tuner.kp_pb = 0.0f;
    mc_session.vel_pid_tuner.ki_pb = 0.000005f;

    mc_session.vel_pid_tuner.p_prime_max = 0.0f;
    mc_session.vel_pid_tuner.i_prime_max = 0.0005f;

    mc_session.controller_configs.hall_a_bias = 0;
    mc_session.controller_configs.hall_b_bias = 0;

    //session.hall_a_coeff = 2.154471544715447;
    //session.hall_b_coeff = 2.154471544715447;

    mc_session.encoder_configs.torque_coeff = 0.06f;

    mc_session.tick_count = 0;
    mc_session.encoder_configs.vel_feedforward = 0.0f;

    // --
    mc_session.flux_cmd_max = 0.95f;
    mc_session.flux_cmd_integral_max = 40.0f;
    mc_session.rot_ang_vel_integral_max = 500.0f;

    mc_session.stop = 0;

    mc_session.encoder_count = 0;

    ab_observer_init(&mc_session.vel_ff_observer, 1.0f, 0.55f, 0.02f);
    ab_observer_init(&mc_session.torque_ff_observer, 1.0f, 0.55f, 0.02f);

    mc_session.vel_filter.a0 = 0.3;
    mc_session.vel_filter.a1 = -0.0;
    mc_session.vel_filter.a2 = 0.0;
    mc_session.vel_filter.b1 =0.7;
    mc_session.vel_filter.b2 = -0.0;
    mc_session.vel_filter.x_1 = 0.0f;
    mc_session.vel_filter.x_2 = 0.0f;
    mc_session.vel_filter.y_1 = 0.0f;
    mc_session.vel_filter.y_2 = 0.0f;

    average_sampler_init(&mc_session.ang_vel_sampler, ANG_VEL_RESAMPLE_AVG);
    average_sampler_init(&mc_session.torque_sampler, TORQUE_RESAMPLE_AVG);
    average_sampler_init(&mc_session.motor_current_sampler, CURRENT_RESAMPLE_AVG);
    average_sampler_init(&mc_session.vel_control_sampler, 2);

    mc_session.controller_configs.dio_config.dir = 0;
    mc_session.dio_state.dio_out = 0;
    mc_session.dio_state.dio_in = 0;

    motion_queue_init(&motion_queue, 512);

    // init filters
    for(int i = 0; i < 10; i++)
    {
    	iir_filter_reset(&mc_session.notch_filters[i]);
    }

    mc_session.enabled_iir_notch_filters = 0;

    mc_session.controller_configs.reference_unit = POS_PER_REVOLUTION;

    mc_session.controller_configs.out_of_control_thres = 1000;

    mc_session.encoder_misscode_count = 0;
    mc_session.encoder_check_enabled = 0;
    mc_session.rot_pos_meas_prev = angular_pos_from_float(0.0f, PERIOD_REVOLUTION);

    // scope default values
	scope_session.var_scope_div = 5;
	scope_session.var_scope_trigger_count = 0;
	scope_session.var_scope_sample_len = 0;
	scope_session.var_scope_triggered = 0;

	scope_session.var_scope_var1 = NULL;
	scope_session.var_scope_var2 = NULL;

	scope_session.count_div = 0;

	// VFD initialize
	mc_session.vfd.electric_angle = 0.0f;
	mc_session.vfd.electric_angle_inc = 0.0f;
	mc_session.vfd.frequency = 0.0f;
	mc_session.vfd.torque_ratio = 0.0f;

	mc_session.set_angle = 0.0f;

    return 0;
}

int initialize_filters(void)
{
    median_filter_init(&mc_session.current_filter_a, 5);
    median_filter_init(&mc_session.current_filter_b, 5);

    mc_session.performance.current = 0;
    mc_session.performance.max = 0;
    mc_session.performance.min = INT32_MAX;

    //float a[] = { 1.0f, -1.778631, 0.80080264 };
    //float b[] = { 1.0f, 2.0f, 1.0f };
    float a[] = { 1.0f, -1.1429805025399, 0.4128015980 };
    float b[] = { 1.0f, 2.0f, 1.0f };
    iir_filter_nth_init(&mc_session.vel_command_filter, a, b, 2);

	return 0;
}
