/*
 * Copyright (c) 2015, 2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "sharc_hw.h"

CircularBuffer uart_tx_fifo;

CircularBuffer *ustdout = &uart_tx_fifo;
CircularBuffer *uart_out = NULL;

void uart_out_redirect(CircularBuffer *fifo)
{
	uart_out = fifo;
}

void dsp_pll_setup(void)
{
	int temp,i;

	 //Step 1 - change the PLLD to 4
	 temp=*pPMCTL;
	 temp&=~PLLD16 ;
	 temp|=(PLLD4 | DIVEN) ;
	 *pPMCTL = temp;

	 //Step 2 - wait for dividers to stabilize
	 for(i=0;i<16;i++);

	 //Step 3 - set INDIV bit now to bring down the VCO speed and enter the bypass mode
	 temp&=~DIVEN;
	 temp|=(INDIV | PLLBP);
	 *pPMCTL = temp;

	 //Step 4 - wait for the PLL to lock
	 for(i=0;i<4096;i++);

	 //Step 5 - come out of the bypass mode
	 temp=*pPMCTL;
	 temp&=~PLLBP;
	 *pPMCTL = temp;

	  //Step 6 - wait for dividers to stabilize
	 for(i=0;i<16;i++);

	  //Step 7 - set the required PLLM and INDIV values here  and enter the bypass mode
	 //PLLM=18, INDIV=0,  fVCO=2*PLLM*CLKIN = 2*18*16.625 = 532 MHz
	 temp = *pPMCTL;
	 temp&=~ (INDIV | PLLM63);
//temp|= (PLLM10| PLLBP);
	 temp|= (PLLM10| PLLBP);
	 *pPMCTL = temp;

	 //Step 8 - wait for the PLL to lock
	 for(i=0;i<4096;i++);

	 //Step 9 - come out of the bypass mode
	 temp = *pPMCTL;
	 temp&=~PLLBP;
	 *pPMCTL=temp;

	 //Step 10 - wait for dividers to stabilize
	 for(i=0;i<16;i++);

	 //Step 11 - set the required values of PLLD(=2) and SDCKR (=2.5) here
	// fCCLK = fVCO/PLLD = 532/2 = 266 MHz, fSDCLK = fCCLK/SDCKR = 266/2 = 133 MHz
	 temp=*pPMCTL;
	 temp&=~(PLLD16 | 0x1C0000 );
	 temp|= (SDCKR2 | PLLD2 | DIVEN);
	 *pPMCTL=temp;

	 //Step 12 - wait for the dividers to stabilize
	 for(i=0;i<16;i++);
}


void dsp_pwm_setup(void)
{
    /* PWM3 */
	/* LIN3: 10, HIN3: 12 */
    SRU(PWM3_AH_O, DPI_PB12_I);
    SRU(HIGH, DPI_PBEN12_I);
    SRU(PWM3_AL_O, DPI_PB10_I);
    SRU(HIGH, DPI_PBEN10_I);

    /* PWM2 */
    /* LIN2: 9, HIN2: 13 */
    SRU(PWM2_AH_O, DPI_PB13_I);
    SRU(HIGH, DPI_PBEN13_I);
    SRU(PWM2_AL_O, DPI_PB09_I);
    SRU(HIGH, DPI_PBEN09_I);

    /* PWM1 */
    /* LIN1: 11, HIN1: 14 */
    SRU(PWM1_AH_O, DPI_PB14_I);
    SRU(HIGH, DPI_PBEN14_I);
    SRU(PWM1_AL_O, DPI_PB11_I);
    SRU(HIGH, DPI_PBEN11_I);

    *pPWMDT3 = 10;
    *pPWMDT2 = 10;
    *pPWMDT1 = 10;

    *pPWMGCTL = (1 << 6) | (1 << 4) | (1 << 2);

    *pPWMCTL3 = 1 << 1;
    *pPWMPERIOD3 = mc_pwm_period;
    *pPWMA3 = 0;

    *pPWMCTL2 = 1 << 1;
    *pPWMPERIOD2 = mc_pwm_period;
    *pPWMA2 = 0;

    *pPWMCTL1 = 1 << 1;
    *pPWMPERIOD1 = mc_pwm_period;
    *pPWMA1 = 0;

    *pSYSCTL |= (1 << 30) | (1 << 27) | (1 << 26) | (1 << 25);
}

void update_pwm_comp(int a, int b, int c)
{
    *pPWMA3 = a - mc_pwm_period_half;
    *pPWMA2 = b - mc_pwm_period_half;
    *pPWMA1 = c - mc_pwm_period_half;
}

void dsp_uart_setup(void)
{
    volatile int temp;
    //int picr2 = 0x0;

    /* maps the UART0 receive interrupt to P14 using the programmable interrupt controller */

    //picr2 = *pPICR2;        /* get PICR2 */
    //picr2 &= (~(0x1f<<10)); /* clear P14 bits */
    //picr2 |= (0x13<<10);    /* set UART0RX */
    //(*pPICR2) = picr2;      /* put it back */

    //sysreg_bit_set(sysreg_IMASK, P14I ); /* unmask UART RX interrupt */


    *pPICR2 &= ~(0x3E0);                                    // Sets the UART0 receive interrupt to P13.
    *pPICR2 |= (0x13<<5);       // For I/O mode, both the transmit and receive interrupt can be
                                                  // programmed through the PICR register using the code select
                                                  // value for the UART receive interrupt (0x13 for UART0 interrupt).

    *pUART0LCR=0;
    *pUART0IER = UARTRBFIE;

    // TX 07 RX 06


/*  (*pUART0IER) = UARTRBFIE;*/     /* enable UART RX interrupt */

    SRU2(UART0_TX_O,DPI_PB07_I);    /* UART TX signal is connected to DPI pin 9 */
    SRU2(HIGH,DPI_PBEN07_I);
    SRU2(DPI_PB06_O,UART0_RX_I);    /* connect the pin buffer output signal to the UART0 RX */
    SRU2(LOW,DPI_PB06_I);
    SRU2(LOW,DPI_PBEN06_I);         /* disable DPI pin10 as input */

    (*pUART0LCR) = UARTDLAB;        /* enables access to divisor register to set baud rate */

    /*Baud rate = PCLK / (16 * Divisor)

where,
      PCLK = CCLK / 2
     Baud rate = required value
     * */

    /* Baud rate: 115200 @ 300MHz */
    //(*pUART0DLL) = 81;
    // 230400
    (*pUART0DLL) = 40;

    (*pUART0DLH) = 0x00;

    (*pUART0LCR) = (UARTWLS8);   /* 8 bit word, odd parity, 2 stop bits */

    (*pUART0RXCTL) = UARTEN;        /* enable UART0 RX */
    (*pUART0TXCTL) = UARTEN;        /* enable UART0 TX */

    int ret = circularBufferInit(&uart_tx_fifo, 16384);

    if(ret == -1)
    {
    	printf("memory allocation failed\n");
    }
}

int __uart_char_send_no_wait(const char c)
{
    int nStatus = 0;
    unsigned int count = 0;

    do
    {
        if((*pUART0LSR & UARTTHRE))
        {
            *pUART0THR = c;
            nStatus = 1;
            break;
        }

        count++;

    } while( count < 0x100000 );

    return nStatus;
}

int uart_char_send(const char cVal)
{
	uint32_t val = cVal;

	/* do not send when no output FIFO is assigned */
	if(!uart_out)
	{
		return -1;
	}

	/* when the FIFO is full, send data in the FIFO first */
	if(circularBufferFull(uart_out))
	{
		uart_process_fifo();
	}

	return circularBufferWrite(uart_out, &val, 1);
}

int uart_process_fifo(void)
{
	uint32_t read_buffer[64];
	int n;

	n = circularBufferRead(uart_out, read_buffer, 64);

	while(n > 0)
	{
        RS485_BUSY();
        for(int i = 0; i < 10000; i++)
        {
            __asm("nop;");
        }

		for(int i = 0; i < n; i++)
		{
			__uart_char_send_no_wait(read_buffer[i]);
		}

        while(!(*pUART0LSR & UARTTHRE));

        for(int i = 0; i < 10000; i++)
        {
            __asm("nop;");
        }

        RS485_IDLE();

		n = circularBufferRead(&uart_tx_fifo, read_buffer, 64);
	}

	return 0;
}

char *uart_str_send(char *str)
{
    while(*str) uart_char_send(*str++);

    return str;
}

void wait(int time)
{
    for(int j = 0; j < time; j++);
}

void dsp_spi_setup(void)
{
    SRU(DPI_PB02_O, SPI_MISO_I);     //Connect DPI PB2 to MISO.
    SRU(SPI_MISO_PBEN_O, DPI_PBEN02_I);

    /* setup SPI signals */
    //SRU(SPI_MOSI_O,DPI_PB01_I);
    //SRU(SPI_MOSI_PBEN_O, DPI_PBEN01_I);

    SRU(SPI_CLK_O,DPI_PB03_I);
    SRU(SPI_CLK_PBEN_O,DPI_PBEN03_I);

    /* for the flag pins to act as chip select */
    SRU(FLAG4_O, DPI_PB05_I);
    SRU(HIGH, DPI_PBEN05_I);

    /* magnetic encoder: FLAG5, DPI04 */
    //SRU(FLAG5_O, DPI_PB04_I);

    //SRU(HIGH, DAI_PBEN08_I);

    /* first set flag 4 as an output */
    sysreg_bit_set(sysreg_FLAGS, FLG4O);
    sysreg_bit_set(sysreg_FLAGS, FLG4);

    sysreg_bit_set(sysreg_FLAGS, FLG5O);
    sysreg_bit_set(sysreg_FLAGS, FLG5);

    /* setup SPI registers */
    *pSPIDMAC = 0;
    *pSPIBAUD = 0;
    *pSPIFLG = 0x8F00;//0xF80;
    //*pSPICTL = 0x400 | (1 << 11);

    *pSPICTL = 0;

    delay(10);

    //*pSPICTL = (SPIEN|SPIMS|SENDZ|TIMOD1|WL8 | GM| (1 << 9) | (0 << 11) | (1 << 10));

    *pSPICTL = (SPIEN|SPIMS|TIMOD1|WL8 | GM| (1 << 9) | (0 << 11) | (1 << 10));

    //printf("%d\n", *pSPICTL);

    *pSPIBAUD = 100;
    *pSPIDMAC = 0;
    *pIISPI = 0;
    *pIMSPI = 0;

    adi_int_EnableInt(ADI_CID_P1I, false);
    adi_int_EnableInt(ADI_CID_P18I, false);
}

int Wait_For_SPIF(void)
{

    int nTimeout = 10000;
    // status updates can be delayed up to 10 cycles
    // so wait at least 10 cycles before even
    // checking them
    int n;

    // make sure nothing is waiting to be sent
    while( !(SPIF & *pSPISTAT) )
    {
//        if( nTimeout-- < 0 )
//        {
//            return 1;
//        }
    	asm("nop;");
    }

    return 0;

}

int Wait_For_SPIF_b(void)
{

    int nTimeout = 10000;
    int n;

    // make sure nothing is waiting to be sent
    while( !(SPIF & *pSPISTATB) )
    {
    	asm("nop;");
    }

    return 0;

}

int spi_byte_write(const int byByte)
{
    int nTimeOut = 100000;
    int n;

    if( 0 != Wait_For_SPIF() )
    {
        return 1;
    }

    while( (TXS & *pSPISTAT) )
    {
        if( nTimeOut-- < 0 )
        {
            return 1;
        }
    }

    asm("nop;");
    asm("nop;");
    asm("nop;");

    *pTXSPI = byByte;

    while( (TXS & *pSPISTAT) )
    {
        if( nTimeOut-- < 0 )
        {
            return 1;
        }
    }

    if( 0 != Wait_For_SPIF() )
    {
        return 1;
    }

    return 0;
}

int spi_byte_write_b(const int byByte)
{
    int nTimeOut = 100000;
    int n;

    if( 0 != Wait_For_SPIF_b() )
    {
        return 1;
    }

    while( (TXS & *pSPISTATB) )
    {
        if( nTimeOut-- < 0 )
        {
            return 1;
        }
    }

    asm("nop;");
    asm("nop;");
    asm("nop;");

    *pTXSPIB = byByte;

    while( (TXS & *pSPISTATB) )
    {
        if( nTimeOut-- < 0 )
        {
            return 1;
        }
    }

    if( 0 != Wait_For_SPIF_b() )
    {
        return 1;
    }

    return 0;
}

int spi_byte_read(int *pbyByte)
{
    int nTimeOut = 1000;

    if( 0 != Wait_For_SPIF() )
    {
        return 1;
    }

    // don't read until there is something to read.
    nTimeOut = 10000;
    while( !(RXS & *pSPISTAT) )
    {
        if( nTimeOut-- < 0 )
        {
            return 1;
        }
    }

    asm("nop;");
    asm("nop;");
    asm("nop;");

    *pbyByte = *pRXSPI;

    return 0;
}

int spi_byte_read_b(int *pbyByte)
{
    int nTimeOut = 1000;

    if( 0 != Wait_For_SPIF_b() )
    {
        return 1;
    }

    // don't read until there is something to read.
    nTimeOut = 10000;
    while( !(RXS & *pSPISTATB) )
    {
        if( nTimeOut-- < 0 )
        {
            return 1;
        }
    }

    asm("nop;");
    asm("nop;");
    asm("nop;");

    *pbyByte = *pRXSPIB;

    return 0;
}

void ad7367_select(void)
{
    /* ADC ready, select it */
   	ADC_CS_LOW();

    /* setup SPI mode */
    *pSPICTL = (SPIEN | SPIMS | TIMOD1 | WL32 | GM | (1 << 9) | (0 << 11) | (0 << 10));
    *pSPIBAUD = 4;
}

void ad7367_release(void)
{
    /* release ADC */
    ADC_CS_HIGH();
    *pSPIBAUD = 0;
}

void adc_read_data(float *channel1, float *channel2)
{
    /* wait for ADC conversion complete */
    while(*pDPI_PIN_STAT & 0x80) { }

    /* ADC ready, select it */
    ADC_CS_LOW();

    /* setup SPI mode */
    ad7367_select();

    /* read */
    int adc_data;
    /* send clock */
    spi_byte_write(0xffff);
    spi_byte_read(&adc_data);

    /* release ADC */
    ad7367_release();

    int ch1 = (adc_data & 0xfffc0000) >> 18;
    int ch2 = (adc_data & 0x0003fff0) >> 4;

    ch2 += ch2 > 8191 ? -8192 : 8192;
    ch1 += ch1 > 8191 ? -8192 : 8192;

    *channel1 = (float)(ch2 - 8192);
    *channel2 = (float)(ch1 - 8192);
}

void delay (int ms)
{
  int i,j;
  for(i=0;i<ms;i++)
  for(j=0;j<10;j++) {asm("nop;");}
}

void _delay_us(unsigned int x)
{
	delay(x);
}

void _delay_ms(unsigned int x)
{
    _delay_us(x);
}

void set_mod_frequency(float freq)
{
	mc_pwm_period = 150000000 / freq;
	mc_pwm_period_half = mc_pwm_period / 2.0;

	*pPWMPERIOD3 = mc_pwm_period;
	*pPWMPERIOD2 = mc_pwm_period;
	*pPWMPERIOD1 = mc_pwm_period;
}
