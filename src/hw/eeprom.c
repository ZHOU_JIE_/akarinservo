/*
 * eeprom.c
 *
 *  Created on: 2016��8��20��
 *      Author: yuchong
 */

#include "sharc_hw.h"

#define WREN 0x06
#define WRDI 0x04
#define RDSR 0x05
#define WRSR 0x01
#define READ 0x03
#define WRITE 0x02

void controller_eeprom_release(void)
{
    SRU(HIGH, DAI_PB20_I);
    *pSPIBAUDB = 0;
}

void controller_eeprom_select(void)
{
    SRU(LOW, DAI_PB20_I);

    *pSPICTLB = (SPIEN|SPIMS|TIMOD1|WL8 | GM| (1 << 9) | (0 << 11) | (0 << 10));

    *pSPIBAUDB = 50;
}

int controller_eeprom_busy(void)
{
	int byte;
    controller_eeprom_select();
    spi_byte_write_b(RDSR);
    spi_byte_write_b(0xff);
    spi_byte_read_b(&byte);
    controller_eeprom_release();

    return byte & 0x01;
}

void controller_eeprom_write_enable(void)
{
	controller_eeprom_select();
    spi_byte_write_b(WREN);
    controller_eeprom_release();
}

void controller_eeprom_write_byte(int address, uint8_t byte)
{
	controller_eeprom_write_enable();

	controller_eeprom_select();
    spi_byte_write_b(WRITE);
    spi_byte_write_b(address);
    spi_byte_write_b(byte);
    controller_eeprom_release();

    while(controller_eeprom_busy());
}

uint8_t controller_eeprom_read_byte(int address)
{
	int byte;
	controller_eeprom_select();
    spi_byte_write_b(READ);
    spi_byte_write_b(address);
    spi_byte_write_b(0xff);
    spi_byte_read_b(&byte);
    controller_eeprom_release();

    return (uint8_t)(byte & 0xff);
}

void controller_eeprom_save(void *encoder, size_t len, int offset)
{
    for(int i = 0; i < len; i++)
    {
    	uint32_t word = *((uint8_t *)(encoder) + i);

    	uint32_t bytes[4];

    	bytes[0] = (word & 0x000000ff);
    	bytes[1] = (word & 0x0000ff00) >> 8;
    	bytes[2] = (word & 0x00ff0000) >> 16;
    	bytes[3] = (word & 0xff000000) >> 24;

    	for(int j = 0; j < 4; j++)
    	{
    		controller_eeprom_write_byte(i * 4 + offset + j, bytes[j]);
    	}
    }
}

void *controller_eeprom_restore(void *encoder, size_t len, int offset)
{
    for(int i = 0; i < len; i++)
    {
    	/* read a word */
    	uint32_t word = 0;
    	for(int j = 0; j < 4; j++)
    	{
    		word <<= 8;
    		word |= controller_eeprom_read_byte(i * 4 + offset + (3 - j));
    	}

    	*((uint8_t *)(encoder) + i) = word;
    }

    return encoder;
}

