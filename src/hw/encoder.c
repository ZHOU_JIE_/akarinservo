/*
 * Copyright (c) 2015, 2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "sharc_hw.h"
#include "encoder.h"

void mag_encoder_select(void)
{
    SRU(LOW, DAI_PB08_I);

    *pSPICTL = (SPIEN|SPIMS|TIMOD1|WL8 | GM| (1 << 9) | (0 << 11) | (1 << 10));

    *pSPIBAUD = 10;
}

void mag_encoder_release(void)
{
    SRU(HIGH, DAI_PB08_I);
    *pSPIBAUD = 0;
}

void mag_encoder_select2(void)
{
    SRU(LOW, DAI_PB08_I);

    *pSPICTLB = (SPIEN|SPIMS|TIMOD1|WL8 | GM| (1 << 9) | (0 << 11) | (1 << 10));

    *pSPIBAUDB = 30;
}

void mag_encoder_release2(void)
{
    SRU(HIGH, DAI_PB08_I);
    *pSPIBAUDB = 0;
}

int motor_control_read_encoder(void)
{
    /*int encoder_upper, encoder_lower;

    //spi_byte_read(&encoder_upper);

    mag_encoder_select();
    {
        spi_byte_write(0xff);
        spi_byte_read(&encoder_upper);
        spi_byte_write(0xff);
        spi_byte_read(&encoder_lower);
    }
    mag_encoder_release();

   return (((encoder_upper & 0x3f) << 8) | encoder_lower) * 0.6103515625f;*/

	mag_encoder_select2();

	int encoder_upper, encoder_lower;

	spi_byte_write_b(0xff);
	spi_byte_read_b(&encoder_upper);
    spi_byte_write_b(0xff);
	spi_byte_read_b(&encoder_lower);

	mag_encoder_release2();

	return (((encoder_upper & 0x3f) << 8) | encoder_lower);// * 0.6103515625f;
}

#define WREN 0x06
#define WRDI 0x04
#define RDSR 0x05
#define WRSR 0x01
#define READ 0x03
#define WRITE 0x02

void mag_eeprom_release(void)
{
    SRU(HIGH, DAI_PB10_I);
    *pSPIBAUD = 0;
}

void mag_eeprom_select(void)
{
    SRU(LOW, DAI_PB10_I);

    *pSPICTLB = (SPIEN|SPIMS|TIMOD1|WL8 | GM| (1 << 9) | (0 << 11) | (0 << 10));

    *pSPIBAUDB = 50;
}

int encoder_eeprom_busy(void)
{
	int byte;
    mag_eeprom_select();
    spi_byte_write_b(RDSR);
    spi_byte_write_b(0xff);
    spi_byte_read_b(&byte);
    mag_eeprom_release();

    return byte & 0x01;
}

void encoder_eeprom_write_enable(void)
{
    mag_eeprom_select();
    spi_byte_write_b(WREN);
    mag_eeprom_release();
}

void encoder_eeprom_write_byte(int address, uint8_t byte)
{
	encoder_eeprom_write_enable();

    mag_eeprom_select();
    spi_byte_write_b(WRITE);
    spi_byte_write_b(address);
    spi_byte_write_b(byte);
    mag_eeprom_release();

    while(encoder_eeprom_busy());
}

uint8_t encoder_eeprom_read_byte(int address)
{
	int byte;
    mag_eeprom_select();
    spi_byte_write_b(READ);
    spi_byte_write_b(address);
    spi_byte_write_b(0xff);
    spi_byte_read_b(&byte);
    mag_eeprom_release();

    return (uint8_t)(byte & 0xff);
}



void encoder_eeprom_save(akarin_encoder_t *encoder, int offset)
{
    for(int i = 0; i < sizeof(akarin_encoder_t); i++)
    {
    	uint32_t word = *((uint8_t *)(encoder) + i);

    	uint32_t bytes[4];

    	bytes[0] = (word & 0x000000ff);
    	bytes[1] = (word & 0x0000ff00) >> 8;
    	bytes[2] = (word & 0x00ff0000) >> 16;
    	bytes[3] = (word & 0xff000000) >> 24;

    	for(int j = 0; j < 4; j++)
    	{
    		encoder_eeprom_write_byte(i * 4 + offset + j, bytes[j]);
    	}
    }
}

akarin_encoder_t *encoder_eeprom_restore(akarin_encoder_t *encoder, int offset)
{
    for(int i = 0; i < sizeof(akarin_encoder_t); i++)
    {
    	/* read a word */
    	uint32_t word = 0;
    	for(int j = 0; j < 4; j++)
    	{
    		word <<= 8;
    		word |= encoder_eeprom_read_byte(i * 4 + offset + (3 - j));
    	}

    	*((uint8_t *)(encoder) + i) = word;
    }

    return encoder;
}

int mag_encoder_detect(void)
{
	if(encoder_eeprom_read_byte(0x00) != 0x00 ||
	   encoder_eeprom_read_byte(0x01) != 0x93 ||
	   encoder_eeprom_read_byte(0x02) != 0x00 ||
       encoder_eeprom_read_byte(0x03) != 0x93)
	{
		return 0;
	}

	return 1;
}
