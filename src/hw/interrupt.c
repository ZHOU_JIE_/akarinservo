/*
 * Copyright (c) 2015, 2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "../motor_control.h"

#define DO_CYCLE_COUNTS
#include <cycle_count.h>

int core_timer_enable = 0;
int uart_rx_idle = 0;

void motion_dio_set_complete(motor_control_session_t *session, int complete)
{
	/* usage register
		 * [0:7] | [8:16] | [17:24] | [25:31]
		 * DIO0  | DIO1   | DIO2    | reserved
		 * */

	uint32_t usage = session->controller_configs.dio_config.usage;

	/* DIO0 */
	if((usage & 0xff) == 0x20)
	{
		if(complete)
		{
			session->dio_state.dio_out |= 0x01;
		}
		else
		{
			session->dio_state.dio_out &= ~0x01;
		}
	}

	/* DIO1 */
	if(((usage >> 8) & 0xff) == 0x20)
	{
		if(complete)
		{
			session->dio_state.dio_out |= 0x02;
		}
		else
		{
			session->dio_state.dio_out &= ~0x02;
		}
	}

	/* DIO2 */
	if(((usage >> 16) & 0xff) == 0x20)
	{
		if(complete)
		{
			session->dio_state.dio_out |= 0x04;
		}
		else
		{
			session->dio_state.dio_out &= ~0x04;
		}
	}
}

void dio_update(void)
{
	int cfg = mc_session.controller_configs.dio_config.dir;
	int out = mc_session.dio_state.dio_out;

#define DIO_OUT_SET(bit, pin) 		\
	do {							\
		if(out & (bit)) 			\
		{ 							\
			SRU(HIGH, pin);			\
		}							\
		else						\
		{							\
			SRU(LOW, pin);			\
		}							\
	} while(0)

	if(cfg & 0x01)
	{
		/* output */
		SRU(HIGH, DAI_PBEN09_I);

		DIO_OUT_SET(0x01, DAI_PB09_I);
	}
	else
	{
		SRU(LOW, DAI_PBEN09_I);
	}

	if(cfg & 0x02)
	{
		SRU(HIGH, DAI_PBEN16_I);

		DIO_OUT_SET(0x02, DAI_PB16_I);
	}
	else
	{
		SRU(LOW, DAI_PBEN16_I);
	}

	if(cfg & 0x04)
	{
		SRU(HIGH, DAI_PBEN12_I);

		DIO_OUT_SET(0x04, DAI_PB12_I);
	}
	else
	{
		SRU(LOW, DAI_PBEN12_I);
	}

#define DIO_IN_READ(bit, pin)						\
	do { 											\
		if((*pDAI_PIN_STAT & pin) > 0)				\
		{											\
			mc_session.dio_state.dio_in |= (bit);	\
		}											\
		else										\
		{											\
			mc_session.dio_state.dio_in &= ~(bit);	\
		}											\
	} while(0)

	/* read IO input */
	DIO_IN_READ(0x01, DAI_PB09);
	DIO_IN_READ(0x02, DAI_PB16);
	DIO_IN_READ(0x04, DAI_PB12);

	/*....*/
	if(mc_session.state == CONTROL_MOTION)
	{
		motion_dio_set_complete(&mc_session, 1);
	}
	if(mc_session.state == CONTROL_POSITION)
	{
		motion_dio_set_complete(&mc_session, 0);
	}
	else
	{
		motion_dio_set_complete(&mc_session, 1);
	}

	// XXX: add homing DIO selection
	if((mc_session.dio_state.dio_in & 0x04))
	{
		mc_session.dio_home_switch = 1;
	}
	else
	{
		mc_session.dio_home_switch = 0;
	}
}

/* core timer interrupt handler */
#pragma optimize_for_speed
#pragma section ("seg_int_code")
void timer_isr(uint32_t iid, void *handlerArg)
{
	dio_update();

	if(mc_session.state == CONTROL_VFD)
	{
		core_timer_enable = 1;
	}

	if(core_timer_enable)
	{
		cycle_t cycle_cnt;
		START_CYCLE_COUNT(cycle_cnt);

		/* execute motor control loop */
		motor_control_execute(&mc_session);

		STOP_CYCLE_COUNT(cycle_cnt, cycle_cnt);

		/* update performance cycle counter */
		if(cycle_cnt > mc_session.performance.max) mc_session.performance.max = cycle_cnt;
		if(cycle_cnt < mc_session.performance.min) mc_session.performance.min = cycle_cnt;
		mc_session.performance.current = cycle_cnt;
	}
} /* timer_isr */

void dsp_core_timer_disable(void)
{
	core_timer_enable = 0;
}

void dsp_core_timer_enable(void)
{
	core_timer_enable = 1;
}

void dsp_core_timer_setup(void)
{
    adi_int_InstallHandler(ADI_CID_TMZHI, timer_isr, NULL, true);

    /* Set tperiod and tcount of the timer. */
    timer_set(14746, 14746);

    /* Start the timer. */
    timer_on();
}

char uart_rx_buffer[UART_RX_BUFFER_SIZE];
int uart_rx_count = 0;

#pragma optimize_for_speed
#pragma section ("seg_int_code")
void uart0_isr(uint32_t iid, void *handlerArg)
{
	/* read byte from FIFO */
    int value = *pUART0RBR;

    /* clear UART timeout timer count by disabling it and enable it again */
    disable_gp_timer();
    enable_gp_timer();

    if(uart_rx_count < UART_RX_BUFFER_SIZE)
    {

#ifdef UART_HANDLE_BACKSPACE
    	if(value == '\b' || value == 127)
    	{
    		if(uart_rx_count != 0)
    		{
        		uart_rx_buffer[uart_rx_count--] = '\0';

        		if(echo_enable)
        		{
        			uart_str_send("\b \b");
        		}
    		}
    	}
    	else

#endif /* UART_HANDLE_BACKSPACE */
    	{
            uart_rx_buffer[uart_rx_count++] = value;

#ifdef UART_HANDLE_ECHO
            if(echo_enable)
            {
            	uart_char_send(value);
            }
#endif /* UART_HANDLE_ECHO */
    	}
    }
}

#pragma optimize_for_speed
#pragma section ("seg_int_code")
static void flag0_isr(uint32_t iid, void *handlerArg)
{

#ifdef ENABLE_PULSE_INPUT
    mc_session.rot_pos_set += sysreg_bit_tst(sysreg_FLAGS, FLG0) == 0 ? MOTOR_STEP_SIZE : -MOTOR_STEP_SIZE;
#endif

    int temp = *pDAI_IRPTL_H;
}

int uart_timeout;

#pragma optimize_for_speed
#pragma section ("seg_int_code")
void gptmr0_isr(uint32_t iid, void *handlerArg)
{
	/* dummy RW1C to service the interrupt */
	int tmp = *pTM0STAT;
	/* disable timer */
	*pTM0STAT = TIM0IRQ | TIM0DIS;

	/* set UART idle flag */
	uart_rx_idle = 1;

	/* not a Modbus package, let main loop to handle it */
    if(uart_rx_buffer[0] == '*')
    {
    	return;
    }

    /* clear state machine states */
    int rx_bytes = uart_rx_count;

    /* make sure main loop does not handle this */
    uart_rx_idle = 0;
    uart_rx_count = 0;

    /* drop the pack if is too long */
    if(rx_bytes >= 255)
    {
        return;
    }

    modbus_handle(uart_rx_buffer, rx_bytes);
}

void init_core_timer(void)
{
        adi_int_InstallHandler(ADI_CID_P0I, /*iid - high priority core timer */
                               flag0_isr,     /*handler*/
                               0, /*handler parameter*/
                               true           /*do enable*/
                              );

        SRU(LOW,DAI_PBEN05_I);
        SRU(LOW,DAI_PB05_I);
        SRU(DAI_PB05_O,DAI_INT_22_I);//DAI PIN14 ���� DAI_INT22
//
//        /* unmask individual interrupts */
        (*pDAI_IRPTL_PRI) |=  DAI_INT_22;
        //*pDAI_IMASK_RE |= DAIHI;
//
//        //(*((volatile unsigned int *)DAI_IRPTL_FE)) &= ~DAI_INT_22;
//
        (*((volatile unsigned int *)DAI_IRPTL_RE))|= DAI_INT_22;
//
//        /* enable IRQ interrupts and make DAI interrupts high priority */
        sysreg_bit_set(sysreg_IMASK, DAIHI );
        sysreg_bit_set(sysreg_MODE1, IRPTEN ); /* enable global interrupts */
}

void init_gp_timer(void)
{
	// bit 3: Count to end of period
	// bit 4: interrupt enable
	*pTMSTAT &= ~TIM0EN;
	*pTM0CTL = TIMODEPWM | PULSE | IRQEN | PRDCNT;
	*pTM0PRD = 1000;
	*pTM0W = 999;

	// timer period
	// The equation for the timer period is: 2 �� (Period Register) �� tPCLK.

	//*pTMSTAT = TIM0EN;

	adi_int_InstallHandler(ADI_CID_P2I, gptmr0_isr, NULL, true);
	sysreg_bit_set(sysreg_IMASK, GPTMR0I);
	adi_int_EnableInt(ADI_CID_P2I, true);
}

void set_gp_timer_expire(int ns)
{
	float s = ns * 1e-9;
	int count = (s * PCLK) / 2.0f;

	*pTM0PRD = count;
	*pTM0W = count - 1;
}

void interrupt_init(void)
{
	init_core_timer();

	init_gp_timer();

    // UART interrupt
    adi_int_InstallHandler(ADI_CID_P13I, uart0_isr, NULL, true);

	sysreg_bit_clr(sysreg_FLAGS, FLG0O);
}
