#include "motion_planner.h"
#include <math.h>

#define M_2PI       6.283185307179586
#define M_HALF_PI   1.570796326794897

static inline float trig_jerk_acc(float duration, float pha)
{
    return (1.0 - sin(M_2PI/ duration * pha + M_HALF_PI));
}

#define STATE_IDLE      -1
#define STATE_ACCELERATE 0
#define STATE_CONST_VEL  1
#define STATE_DECELERATE 2

motion_controller_t *motion_controller_init(motion_controller_t *controller)
{
    controller->state = STATE_IDLE;
     controller-> mc_tick = 0;

     controller-> vel_demand = 0.0f;
     controller-> acc_demand = 0.0f;
     controller-> pos_demand = 0.0f;


     controller-> direction = 1;

    return controller;
}

motion_controller_t *motion_control_tick(motion_controller_t *c)
{
    switch (c->state)
    {
        case STATE_ACCELERATE:
        {
            c->acc_demand = c->acc_peak * trig_jerk_acc(c->ramp_duration, c->mc_tick);
            c->vel_demand += c->acc_demand;
            c->pos_demand += c->vel_demand;

            if(c->mc_tick >= c->ramp_duration - 1)
            {
                c->const_acc_section = (c->move_amount - c->pos_demand * 2) / c->vel_demand;
                c->state = STATE_CONST_VEL;
            }
            break;
        }
        case STATE_CONST_VEL:
        {
            c->pos_demand += c->vel_demand;

            if(c->mc_tick - c->ramp_duration > c->const_acc_section)
            {
                c->state = STATE_DECELERATE;
            }
            break;
        }
        case STATE_DECELERATE:
        {
            int time = c->mc_tick - c->ramp_duration -
            (c->const_acc_section > 0 ? c->const_acc_section : 0);

            c->acc_demand = -c->acc_peak * trig_jerk_acc(c->ramp_duration, time);
            c->vel_demand += c->acc_demand;
            c->pos_demand += c->vel_demand;

            if(c->mc_tick > c->ramp_duration * 2 + c->const_acc_section)
            {
                c->state = STATE_IDLE;
                return NULL;
            }
            break;
        }

        default:
            break;
    }

    c->pos_output = c->current_pos + c->pos_demand * c->direction;

    c->mc_tick++;

    return c;
}

motion_controller_t *motion_prepare(motion_controller_t *c,
                                    float current_pos, float target_pos,
                                    float acc_peak, float vel_peak)
{
    motion_controller_init(c);

    c->current_pos = current_pos;
    c->target_pos = target_pos;

    c-> acc_peak     = acc_peak / 2.0;
    c-> vel_peak      = vel_peak;

    c->move_amount = c->target_pos - c->current_pos;


    if(c->move_amount < 0)
    {
        c->direction = -1;
        c->move_amount = -c->move_amount;
    }
    else
    {
        c->direction = 1;
    }

    float acc_test = (sqrtf(c->acc_peak) * sqrtf(c->move_amount));

    c->vel_peak = c->vel_peak < acc_test ? c->vel_peak : acc_test;

    c->ramp_duration = c->vel_peak / c->acc_peak;

    c->state = STATE_ACCELERATE;

    return c;
}
